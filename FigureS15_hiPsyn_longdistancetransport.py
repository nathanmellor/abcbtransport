import os

from vroot.simulation import Simulation
from models.auxintransport_ABCB_ode_noBC import auxintransport_ABCB_ode_noBC


from models.set_ABCB_carriers import set_ABCB_carriers,simple_knockout

import pickle

timestep=20
nsteps=180

auxin_at_vasc = {}
auxin_at_tip = {}


for gt in ['wt','a1','a19','a1a19']:


    auxin_at_tip[gt]=[]
    for i in range(1,6):
        tissuename = "Tissues/DIIV_mock_050517.zip"
        sim=Simulation(tissuename,'rootward_%s_scen%d_adj_hiPsyn' % (gt,i),biochemical=auxintransport_ABCB_ode_noBC,output=False)
        sim.set_param_values({'ABCB_scen':i,'P_PABCB':3.0})
        sim.reset_time()
        db=sim.get_db()

        auxin=db.get_property('auxin')
        cell_type=db.get_property('cell_type')
        zones=db.get_property('zones')

        auxin_wall=db.get_property('auxin_wall')

        border=db.get_property('border')

        A1=db.get_property('ABCB1')
        A19=db.get_property('ABCB19')
        A4=db.get_property('ABCB4')

        
        if gt=='a1':
            for eid,val in A4.items():
                A1[eid]=0.0
                A4[eid]=val*1.402
                A19[eid]=A19[eid]*1.292
                
        if gt=='a1a19':
            for eid,val in A4.items():
                A1[eid]=0.0
                A19[eid]=0.0
                A4[eid]=val*1.299
                
        if gt=='a19':
            for eid,val in A19.items():
                A19[eid]=0.0
                A1[eid]=val*1.256
                A4[eid]=A4[eid]*1.814
                
        for wid in auxin_wall.keys():
            auxin_wall[wid]=0.001

        for cid,val in auxin.items():
            if cell_type[cid] in [5,16] and cid in border.keys():
                auxin[cid]=1.0
            else:
                auxin[cid]=0.0001

        sim.set_timestep(timestep)
        sim.run_simulation(nsteps)
        os.system('mkdir -p %s' % sim.dir_name)
        sim.plot_figure('auxin',{'auxin':[0,0.1]},(1,1))
        zones=db.get_property('zones')
        V=db.get_property('V')
        auxin_at_tip[gt].append(sum([V[cid]*val for cid,val in auxin.items() if zones[cid]<2]))
        
for gt in ['wt','a4']:

    auxin_at_vasc[gt]=[]

    for i in range(1,6):

        
        sim=Simulation(tissuename,'shootward_%s_scen%d_adj_hiPsyn' % (gt,i),biochemical=auxintransport_ABCB_ode_noBC,output=False)
        sim.set_param_values({'ABCB_scen':i,'P_PABCB':3.0})
        sim.reset_time()
        db=sim.get_db()

        auxin=db.get_property('auxin')
        cell_type=db.get_property('cell_type')
        zones=db.get_property('zones')

        auxin_wall=db.get_property('auxin_wall')

        border=db.get_property('border')
        
        
        A1=db.get_property('ABCB1')
        A19=db.get_property('ABCB19')
        A4=db.get_property('ABCB4')

                
        if gt=='a4':
            for eid,val in A1.items():
                A4[eid]=0.0
                A1[eid]=val*1.240
                A19[eid]=A19[eid]*1.336
                

                
        for wid in auxin_wall.keys():
            auxin_wall[wid]=0.001

        
        for cid,val in auxin.items():
            if cell_type[cid] in [6,15] and zones[cid]==0:
                auxin[cid]=1.0
            else:
                auxin[cid]=0.0001
        

        sim.set_timestep(timestep)
        sim.run_simulation(nsteps)
        os.system('mkdir -p %s' % sim.dir_name)
        sim.plot_figure('auxin',{'auxin':[0,0.1]},(1,1))
        V=db.get_property('V')
        border=db.get_property('border')
        auxin_at_vasc[gt].append(sum([V[cid]*val for cid,val in auxin.items() if cid in border.keys()]))



pickle.dump(auxin_at_vasc,open('output/auxin_vasc_adjExp_hiPsyn.zip','wb'))
pickle.dump(auxin_at_tip,open('output/auxin_tip_adjExp_hiPsyn.zip','wb'))



from openalea.celltissue import TissueDB


from vroot.pylab_plot import plot_pylab_figure,plotAuxinFlux

from pylab import *
import matplotlib.pyplot as plt
from numpy import arange
import pickle
    
from matplotlib import rc


#auxin_at_tip=pickle.load(open('output/auxin_vasc_adjExp_hiPsyn.zip','rb'))
#print auxin_at_tip
#auxin_at_vasc=pickle.load(open('output/auxin_tip_adjExp_hiPsyn.zip','rb'))
#print(auxin_at_vasc)


# activate latex text rendering
rc('text', usetex=True)


#plot bars

fig, ax = plt.subplots(figsize=(4,4.5),dpi=160)
ind=arange(5)
width=0.4
ax.bar(ind-width,auxin_at_vasc['wt'],width,color='k',label='wt')
ax.bar(ind,auxin_at_vasc['a4'],width,color='g',label=r'\textit{abcb4}')


ax.set_xticks(ind-width/2.0)
ax.set_ylabel('auxin at boundary (1 hour)', color='k',fontsize=18)
ax.set_xticklabels(('I', 'II', 'III','IV','V'),fontsize=18)
ax.legend(loc='upper right',fontsize=16)



#save and close
savefig('output/longdistancevasc_adjExp_hiPsyn.png',facecolor=fig.get_facecolor())
clf()

close(fig)




#plot bars

fig, ax = plt.subplots(figsize=(7,4.5),dpi=160)
ind=arange(5)
width=0.2
ax.bar(ind-2*width,auxin_at_tip['wt'],width,color='k',label='wt')
ax.bar(ind-width,auxin_at_tip['a1'],width,color='b',label=r'\textit{abcb1}')
ax.bar(ind,auxin_at_tip['a19'],width,color='g',label=r'\textit{abcb19}')
ax.bar(ind+width,auxin_at_tip['a1a19'],width,color='r',label=r'\textit{abcb1/19}')

ax.set_xticks(ind-width/2.0)
ax.set_ylabel('auxin at tip (1 hour)', color='k',fontsize=18)
ax.set_xticklabels(('I', 'II', 'III','IV','V'),fontsize=18)
ax.legend()

#fig.subplots_adjust(bottom=0.25)

#save and close
savefig('output/longdistancetip_adjExp_hiPsyn.png',facecolor=fig.get_facecolor())
clf()

close(fig)

