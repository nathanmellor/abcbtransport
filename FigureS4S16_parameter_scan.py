
from vroot.simulation import Simulation

from models.auxintransport_ABCB import auxintransport_ABCB
from numpy import mean

from models.set_ABCB_carriers import set_ABCB_carriers,simple_knockout
from pylab import *
import matplotlib.pyplot as plt
from scipy.stats import sem

def compare_with_data(db):

    cell_centres=db.get_property('cell_centres')
    cell_type=db.get_property('cell_type')
    VENUS=db.get_property('VENUS')
    DII=db.get_property('nuclei_data')
    zones=db.get_property('zones')
    epi_model_mz=[]
    epi_data_mz=[]
    cor_model_mz=[]
    cor_data_mz=[]
    epi_model_ez=[]
    epi_data_ez=[]
    cor_model_ez=[]
    cor_data_ez=[]
    for cid,val in cell_type.items():
        if val==2:
            if zones[cid]>3:
                epi_model_ez.append(VENUS[cid])
                epi_data_ez.append(DII[cid])
            else:
                epi_model_mz.append(VENUS[cid])
                epi_data_mz.append(DII[cid])
        if val==4:
            if zones[cid]>4:
                cor_model_ez.append(VENUS[cid])
                cor_data_ez.append(DII[cid])
            else:
                cor_model_mz.append(VENUS[cid])
                cor_data_mz.append(DII[cid])

    model_means=[mean(epi_model_ez),mean(epi_model_mz),mean(cor_model_ez),mean(cor_model_mz)]
    model_errors=[sem(epi_model_ez),sem(epi_model_mz),sem(cor_model_ez),sem(cor_model_mz)]
    model_mean_plus_error=[val+model_errors[i] for i,val in enumerate(model_means)]
    model_means=[val/max(model_mean_plus_error) for val in model_means]
    model_errors=[val/max(model_mean_plus_error) for val in model_errors]
    #print 'mm',model_means

    data_means=[mean(epi_data_ez),mean(epi_data_mz),mean(cor_data_ez),mean(cor_data_mz)]
    data_errors=[sem(epi_data_ez),sem(epi_data_mz),sem(cor_data_ez),sem(cor_data_mz)]
    data_mean_plus_error=[val+data_errors[i] for i,val in enumerate(data_means)]
    data_means=[val/max(data_mean_plus_error) for val in data_means]
    data_errors=[val/max(data_mean_plus_error) for val in data_errors]
    #print 'mm',data_means
    #return sum([((data_means[i]-model_means[i])**2)/(data_errors[i]*model_errors[i]) for i in range(4)])
    return sum([((data_means[i]-model_means[i])**2) for i in range(4)])
step=0.1
samples=40

cm_to_inch=0.394

for pname in ['PABCB','P_PABCB','PPIN','PAUX1','Pplas','PLAX']:
    vals={}
    for i in range(1,6):
        print('********',i)
        vals[i]=[]
        pvals=[]
        for pval in range(samples+1):
            tissuename = "Tissues/DIIV_mock_271017_s002.zip"
            sim=Simulation(tissuename,'pscanf',biochemical=auxintransport_ABCB, output = False)
            db=sim.get_db()
            set_ABCB_carriers(db)
            sim.set_param_values({'ABCB_scen':i,pname:step*pval})
            sim.run_simulation(1)

            vals[i].append(compare_with_data(db))
            pvals.append(pval*step)
    fig, ax1 = plt.subplots(figsize=(8.0*cm_to_inch,6.0*cm_to_inch))
    cols='rgbkm'
    for i in range(1,6):
        ax1.plot(pvals,vals[i],cols[i-1],label='sc %s' % i)
    ax1.set_xlabel(pname,FontSize=10)
    ax1.set_ylabel('F',FontSize=10)
    #ax1.legend()
    #ax1.set_title(pname,FontSize=10)
    plt.gcf().subplots_adjust(left=0.20,bottom=0.2)
    savefig('output/Pscan_%s' % pname)
    clf()
    close(fig)


for pname in ['PABCB','P_PABCB','PPIN','PAUX1','Pplas','PLAX']:
    vals={}
    for i in range(1,6):
        print('********',i)
        vals[i]=[]
        pvals=[]
        for pval in range(samples+1):
            tissuename = "Tissues/b1.zip"
            sim=Simulation(tissuename,'pscanf',biochemical=auxintransport_ABCB, output = False)
            db=sim.get_db()
            set_ABCB_carriers(db)
            simple_knockout(db,'ABCB1')
            sim.set_param_values({'ABCB_scen':i,pname:step*pval})
            sim.run_simulation(1)

            vals[i].append(compare_with_data(db))
            pvals.append(pval*step)
    fig, ax1 = plt.subplots(figsize=(8.0*cm_to_inch,6.0*cm_to_inch))
    cols='rgbkm'
    for i in range(1,6):
        ax1.plot(pvals,vals[i],cols[i-1],label='sc %s' % i)
    ax1.set_xlabel(pname,FontSize=10)
    ax1.set_ylabel('F',FontSize=10)
    #ax1.legend()
    #ax1.set_title(pname,FontSize=10)
    plt.gcf().subplots_adjust(left=0.20,bottom=0.2)
    savefig('output/b1_Pscan_%s' % pname)
    clf()
    close(fig)
    
for pname in ['PABCB','P_PABCB','PPIN','PAUX1','Pplas','PLAX']:
    vals={}
    for i in range(1,6):
        print('********',i)
        vals[i]=[]
        pvals=[]
        for pval in range(samples+1):
            tissuename = "Tissues/b4.zip"
            sim=Simulation(tissuename,'pscanf',biochemical=auxintransport_ABCB, output = False)
            db=sim.get_db()
            set_ABCB_carriers(db)
            simple_knockout(db,'ABCB4')
            sim.set_param_values({'ABCB_scen':i,pname:step*pval})
            sim.run_simulation(1)

            vals[i].append(compare_with_data(db))
            pvals.append(pval*step)
    fig, ax1 = plt.subplots(figsize=(8.0*cm_to_inch,6.0*cm_to_inch))
    cols='rgbkm'
    for i in range(1,6):
        ax1.plot(pvals,vals[i],cols[i-1],label='sc %s' % i)
    ax1.set_xlabel(pname,FontSize=10)
    ax1.set_ylabel('F',FontSize=10)
    #ax1.legend()
    #ax1.set_title(pname,FontSize=10)
    plt.gcf().subplots_adjust(left=0.20,bottom=0.2)
    savefig('output/b4_Pscan_%s' % pname)
    clf()
    close(fig)
    
    
for pname in ['PABCB','P_PABCB','PPIN','PAUX1','Pplas','PLAX']:
    vals={}
    for i in range(1,6):
        print('********',i)
        vals[i]=[]
        pvals=[]
        for pval in range(samples+1):
            tissuename = "Tissues/b19.zip"
            sim=Simulation(tissuename,'pscanf',biochemical=auxintransport_ABCB, output = False)
            db=sim.get_db()
            set_ABCB_carriers(db)
            simple_knockout(db,'ABCB19')
            sim.set_param_values({'ABCB_scen':i,pname:step*pval})
            sim.run_simulation(1)

            vals[i].append(compare_with_data(db))
            pvals.append(pval*step)
    fig, ax1 = plt.subplots(figsize=(8.0*cm_to_inch,6.0*cm_to_inch))
    cols='rgbkm'
    for i in range(1,6):
        ax1.plot(pvals,vals[i],cols[i-1],label='sc %s' % i)
    ax1.set_xlabel(pname,FontSize=10)
    ax1.set_ylabel('F',FontSize=10)
    #ax1.legend()
    #ax1.set_title(pname,FontSize=10)
    plt.gcf().subplots_adjust(left=0.20,bottom=0.2)
    savefig('output/b19_Pscan_%s' % pname)
    clf()
    close(fig)

for pname in ['PABCB','P_PABCB','PPIN','PAUX1','Pplas','PLAX']:
    vals={}
    for i in range(1,6):
        print('********',i)
        vals[i]=[]
        pvals=[]
        for pval in range(samples+1):
            tissuename = "Tissues/b4-1b19-1_april2019_rep1.zip"
            sim=Simulation(tissuename,'pscanf',biochemical=auxintransport_ABCB, output = False)
            db=sim.get_db()
            set_ABCB_carriers(db)
            simple_knockout(db,'ABCB4')
            simple_knockout(db,'ABCB19')
            sim.set_param_values({'ABCB_scen':i,pname:step*pval})
            sim.run_simulation(1)

            vals[i].append(compare_with_data(db))
            pvals.append(pval*step)
    fig, ax1 = plt.subplots(figsize=(8.0*cm_to_inch,6.0*cm_to_inch))
    cols='rgbkm'
    for i in range(1,6):
        ax1.plot(pvals,vals[i],cols[i-1],label='sc %s' % i)
    ax1.set_xlabel(pname,FontSize=10)
    ax1.set_ylabel('F',FontSize=10)
    #ax1.legend()
    #ax1.set_title(pname,FontSize=10)
    plt.gcf().subplots_adjust(left=0.20,bottom=0.2)
    savefig('output/b4b19_Pscan_%s' % pname)
    clf()
    close(fig)
 
for pname in ['PABCB','P_PABCB','PPIN','PAUX1','Pplas','PLAX']:
    vals={}
    for i in range(1,6):
        print('********',i)
        vals[i]=[]
        pvals=[]
        for pval in range(samples+1):
            tissuename = "Tissues/b4-1b1-100_april2019_rep2.zip"
            sim=Simulation(tissuename,'pscanf',biochemical=auxintransport_ABCB, output = False)
            db=sim.get_db()
            set_ABCB_carriers(db)
            simple_knockout(db,'ABCB1')
            simple_knockout(db,'ABCB4')
            sim.set_param_values({'ABCB_scen':i,pname:step*pval})
            sim.run_simulation(1)
            newval=compare_with_data(db)
            vals[i].append(newval)
            pvals.append(pval*step)
    fig, ax1 = plt.subplots(figsize=(8.0*cm_to_inch,6.0*cm_to_inch))
    cols='rgbkm'
    for i in range(1,6):
        ax1.plot(pvals,vals[i],cols[i-1],label='sc %s' % i)
    ax1.set_xlabel(pname,FontSize=10)
    ax1.set_ylabel('F',FontSize=10)
    #ax1.legend()
    #ax1.set_title(pname,FontSize=10)
    plt.gcf().subplots_adjust(left=0.20,bottom=0.2)
    savefig('output/b1b4_Pscan_%s' % pname)
    clf()
    close(fig)
    

for pname in ['PABCB','P_PABCB','PPIN','PAUX1','Pplas','PLAX']:
    vals={}
    for i in range(1,6):
        print('********',i)
        vals[i]=[]
        pvals=[]
        for pval in range(samples+1):
            tissuename = "Tissues/b1-100b19-1_april2019_rep1.zip"
            sim=Simulation(tissuename,'pscanf',biochemical=auxintransport_ABCB, output = False)
            db=sim.get_db()
            set_ABCB_carriers(db)
            simple_knockout(db,'ABCB1')
            simple_knockout(db,'ABCB19')
            sim.set_param_values({'ABCB_scen':i,pname:step*pval})
            sim.run_simulation(1)

            vals[i].append(compare_with_data(db))
            pvals.append(pval*step)
    fig, ax1 = plt.subplots(figsize=(8.0*cm_to_inch,6.0*cm_to_inch))
    cols='rgbkm'
    for i in range(1,6):
        ax1.plot(pvals,vals[i],cols[i-1],label='sc %s' % i)
    ax1.set_xlabel(pname,FontSize=10)
    ax1.set_ylabel('F',FontSize=10)
    #ax1.legend()
    #ax1.set_title(pname,FontSize=10)
    plt.gcf().subplots_adjust(left=0.20,bottom=0.2)
    savefig('output/b1b19_Pscan_%s' % pname)
    clf()
    close(fig)

