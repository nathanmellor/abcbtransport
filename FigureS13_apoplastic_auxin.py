

from models.auxintransport_ABCB import auxintransport_ABCB
from vroot.simulation import Simulation
from models.set_ABCB_carriers import set_ABCB_carriers,simple_knockout

for gt in ['wt','a1','a4','a19','a1a19','a4a19','a1a4']:
    for i in range(1,6):
        sim=Simulation("Tissues/DIIV_mock_271017_s002.zip",'wall_scenario_%d_%s' % (i,gt),biochemical=auxintransport_ABCB, output = False)
        db=sim.get_db()
        set_ABCB_carriers(db)
        sim.set_param_values({'ABCB_scen':i})
        if gt=='a1':
            simple_knockout(db,'ABCB1')
        if gt=='a4':
            simple_knockout(db,'ABCB4')
        if gt=='a19':
            simple_knockout(db,'ABCB19')
        if gt=='a4a19':
            simple_knockout(db,'ABCB4')
            simple_knockout(db,'ABCB19') 
        if gt=='a1a19':
            simple_knockout(db,'ABCB1')
            simple_knockout(db,'ABCB19') 
        if gt=='a1a4':
            simple_knockout(db,'ABCB1')
            simple_knockout(db,'ABCB4')
        sim.run_simulation(1)
        sim.plot_figure('auxin_wall',{'auxin_wall':[0,40.0]},(1,1))

