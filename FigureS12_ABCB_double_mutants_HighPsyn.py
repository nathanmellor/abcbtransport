
from vroot.simulation import Simulation

from models.auxintransport_ABCB import auxintransport_ABCB
from numpy import mean

from models.set_ABCB_carriers import set_ABCB_carriers, simple_knockout
from pylab import *
import matplotlib.pyplot as plt
from scipy.stats import sem

def plot_celltypezone_VENUS(db,savename):

    cell_centres=db.get_property('cell_centres')
    cell_type=db.get_property('cell_type')
    VENUS=db.get_property('VENUS')
    DII=db.get_property('nuclei_data')
    zones=db.get_property('zones')
    epi_model_mz=[]
    epi_data_mz=[]
    cor_model_mz=[]
    cor_data_mz=[]
    epi_model_ez=[]
    epi_data_ez=[]
    cor_model_ez=[]
    cor_data_ez=[]
    for cid,val in cell_type.items():
        if val==2:
            if zones[cid]>3:
                epi_model_ez.append(VENUS[cid])
                epi_data_ez.append(DII[cid])
            else:
                epi_model_mz.append(VENUS[cid])
                epi_data_mz.append(DII[cid])
        if val==4:
            if zones[cid]>4:
                cor_model_ez.append(VENUS[cid])
                cor_data_ez.append(DII[cid])
            else:
                cor_model_mz.append(VENUS[cid])
                cor_data_mz.append(DII[cid])
    cm_to_inch=1.0
    fig, ax1 = plt.subplots(figsize=(4.3*cm_to_inch,3.4*cm_to_inch))
    ind=arange(4)
    width=0.35
    ax1.bar(ind-width,[mean(epi_model_ez),mean(epi_model_mz),mean(cor_model_ez),mean(cor_model_mz)],width,color='b')
    ax1.errorbar(ind-width,[mean(epi_model_ez),mean(epi_model_mz),mean(cor_model_ez),mean(cor_model_mz)],yerr=[sem(epi_model_ez),sem(epi_model_mz),sem(cor_model_ez),sem(cor_model_mz)],fmt='none',ecolor='k',capsize=3)


    ax1.set_ylabel('Model', color='b')
    ax1.tick_params('y', colors='b')

    ax2 = ax1.twinx()

    ax2.bar(ind,[mean(epi_data_ez),mean(epi_data_mz),mean(cor_data_ez),mean(cor_data_mz)],width,color='r')
    ax2.errorbar(ind,[mean(epi_data_ez),mean(epi_data_mz),mean(cor_data_ez),mean(cor_data_mz)],yerr=[sem(epi_data_ez),sem(epi_data_mz),sem(cor_data_ez),sem(cor_data_mz)],fmt='none',ecolor='k',capsize=3)
    ax2.set_ylabel('Data', color='r')
    ax2.tick_params('y', colors='r')

    fig.tight_layout()


    #save and close
    savefig(savename,facecolor=fig.get_facecolor())
    clf()

    close(fig)



#only want scenario 3 here
for i in [3]:
    print('********',i)
    
    tissuename = "Tissues/b4-1b19-1_april2019_rep1.zip"


    sim=Simulation(tissuename,'ADJscenario_%d_highPsyn' % i,biochemical=auxintransport_ABCB, output = False)
    db=sim.get_db()
    set_ABCB_carriers(db)
    simple_knockout(db,'ABCB4')
    simple_knockout(db,'ABCB19')

    ABCB1=db.get_property('ABCB1')
    for eid,val in ABCB1.items():
        ABCB1[eid]=val*1.451


    sim.set_param_values({'ABCB_scen':i,'P_PABCB':3.0})
    sim.run_simulation(1)
    plot_celltypezone_VENUS(db,'output/ADJbar_venus_s%d_a4a19_highPsyn.png' % i)

    sim.plot_figure('auxin',{'auxin':[0,sim.get_95pc('auxin')]},(1,1))
    sim.plot_figure('VENUS',{'VENUS':[0,sim.get_95pc('VENUS')]},(1,1))
    sim.plot_figure('nuclei_data',{'nuclei_data':[0,sim.get_95pc('nuclei_data')]},(1,1))
    sim.plot_flux('combined_flux','combined_flux',0.5,0.5,'b')


    tissuename = "Tissues/b1-100b19-1_april2019_rep1.zip"

    sim=Simulation(tissuename,'ADJscenario_%d_highPsyn' % i,biochemical=auxintransport_ABCB, output = False)
    db=sim.get_db()
    set_ABCB_carriers(db)
    simple_knockout(db,'ABCB1')
    simple_knockout(db,'ABCB19')

    ABCB4=db.get_property('ABCB4')
    for eid,val in ABCB4.items():
        ABCB4[eid]=val*1.299

    sim.set_param_values({'ABCB_scen':i,'P_PABCB':3.0})
    sim.run_simulation(1)
    
    plot_celltypezone_VENUS(db,'output/ADJbar_venus_s%d_a1a19_highPsyn.png' % i)
    sim.plot_figure('auxin',{'auxin':[0,sim.get_95pc('auxin')]},(1,1))
    sim.plot_figure('VENUS',{'VENUS':[0,sim.get_95pc('VENUS')]},(1,1))
    sim.plot_figure('nuclei_data',{'nuclei_data':[0,sim.get_95pc('nuclei_data')]},(1,1))
    sim.plot_flux('combined_flux','combined_flux',0.5,0.5,'b')

    
    tissuename = "Tissues/b4-1b1-100_april2019_rep2.zip"

    sim=Simulation(tissuename,'ADJscenario_%d_highPsyn' % i,biochemical=auxintransport_ABCB, output = False)
    db=sim.get_db()
    set_ABCB_carriers(db)
    simple_knockout(db,'ABCB1')
    simple_knockout(db,'ABCB4')


    ABCB19=db.get_property('ABCB19')
    for eid,val in ABCB19.items():
        ABCB19[eid]=val*2.317

    sim.set_param_values({'ABCB_scen':i,'P_PABCB':3.0})
    sim.run_simulation(1)
    
    plot_celltypezone_VENUS(db,'output/ADJbar_venus_s%d_a1a4_highPsyn.png' % i)
    sim.plot_figure('auxin',{'auxin':[0,sim.get_95pc('auxin')]},(1,1))
    sim.plot_figure('VENUS',{'VENUS':[0,sim.get_95pc('VENUS')]},(1,1))
    sim.plot_figure('nuclei_data',{'nuclei_data':[0,sim.get_95pc('nuclei_data')]},(1,1))
    sim.plot_flux('combined_flux','combined_flux',0.5,0.5,'b')


