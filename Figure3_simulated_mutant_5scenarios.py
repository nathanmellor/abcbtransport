
from vroot.simulation import Simulation


from models.auxintransport_ABCB import auxintransport_ABCB
from models.set_ABCB_carriers import set_ABCB_carriers,simple_knockout


for i in range(1,6):
    print('********',i)

    # abcb1
    tissuename = "Tissues/DIIV_mock_271017_s002.zip"
    sim=Simulation(tissuename,'abcb1_scenario_%d' % i,biochemical=auxintransport_ABCB, output = False)
    db=sim.get_db()
    set_ABCB_carriers(db)
    simple_knockout(db,'ABCB1')
    sim.set_param_values({'ABCB_scen':i})
    sim.run_simulation(1)
    sim.plot_figure('auxin',{'auxin':[0,sim.get_95pc('auxin')]},(1,1))
    sim.plot_figure('VENUS',{'VENUS':[0,sim.get_95pc('VENUS')]},(1,1))
    sim.plot_flux('combined_flux','combined_flux',0.5,0.5,'b')

    # abcb4
    tissuename = "Tissues/DIIV_mock_271017_s002.zip"
    sim=Simulation(tissuename,'abcb4_scenario_%d' % i,biochemical=auxintransport_ABCB, output = False)
    db=sim.get_db()
    set_ABCB_carriers(db)
    simple_knockout(db,'ABCB4')
    sim.set_param_values({'ABCB_scen':i})
    sim.run_simulation(1)
    sim.plot_figure('auxin',{'auxin':[0,sim.get_95pc('auxin')]},(1,1))
    sim.plot_figure('VENUS',{'VENUS':[0,sim.get_95pc('VENUS')]},(1,1))
    sim.plot_flux('combined_flux','combined_flux',0.5,0.5,'b')

    # abcb19
    tissuename = "Tissues/DIIV_mock_271017_s002.zip"
    sim=Simulation(tissuename,'abcb19_scenario_%d' % i,biochemical=auxintransport_ABCB, output = False)
    db=sim.get_db()
    set_ABCB_carriers(db)
    simple_knockout(db,'ABCB19')
    sim.set_param_values({'ABCB_scen':i})
    sim.run_simulation(1)
    sim.plot_figure('auxin',{'auxin':[0,sim.get_95pc('auxin')]},(1,1))
    sim.plot_figure('VENUS',{'VENUS':[0,sim.get_95pc('VENUS')]},(1,1))
    sim.plot_flux('combined_flux','combined_flux',0.5,0.5,'b')

    # abcb4abcb19
    tissuename = "Tissues/DIIV_mock_271017_s002.zip"
    sim=Simulation(tissuename,'abcb4abcb19_scenario_%d' % i,biochemical=auxintransport_ABCB, output = False)
    db=sim.get_db()
    set_ABCB_carriers(db)
    simple_knockout(db,'ABCB4')
    simple_knockout(db,'ABCB19')
    
    sim.set_param_values({'ABCB_scen':i})
    sim.run_simulation(1)
    sim.plot_figure('auxin',{'auxin':[0,sim.get_95pc('auxin')]},(1,1))
    sim.plot_figure('VENUS',{'VENUS':[0,sim.get_95pc('VENUS')]},(1,1))

    sim.plot_flux('combined_flux','combined_flux',0.5,0.5,'b')

    # abcb1abcb19
    tissuename = "Tissues/DIIV_mock_271017_s002.zip"
    sim=Simulation(tissuename,'abcb1abcb19_scenario_%d' % i,biochemical=auxintransport_ABCB, output = False)
    db=sim.get_db()
    set_ABCB_carriers(db)
    simple_knockout(db,'ABCB1')
    simple_knockout(db,'ABCB19')
    
    sim.set_param_values({'ABCB_scen':i})
    sim.run_simulation(1)
    sim.plot_figure('auxin',{'auxin':[0,sim.get_95pc('auxin')]},(1,1))
    sim.plot_figure('VENUS',{'VENUS':[0,sim.get_95pc('VENUS')]},(1,1))
    sim.plot_flux('combined_flux','combined_flux',0.5,0.5,'b')
    
    # abcb1abcb4
    tissuename = "Tissues/DIIV_mock_271017_s002.zip"
    sim=Simulation(tissuename,'abcb1abcb4_scenario_%d' % i,biochemical=auxintransport_ABCB, output = False)
    db=sim.get_db()
    set_ABCB_carriers(db)
    simple_knockout(db,'ABCB1')
    simple_knockout(db,'ABCB4')

    sim.set_param_values({'ABCB_scen':i})
    sim.run_simulation(1)
    sim.plot_figure('auxin',{'auxin':[0,sim.get_95pc('auxin')]},(1,1))
    sim.plot_figure('VENUS',{'VENUS':[0,sim.get_95pc('VENUS')]},(1,1))
    sim.plot_flux('combined_flux','combined_flux',0.5,0.5,'b')

