
from openalea.celltissue import TissueDB
from vroot.simulation import Simulation
from models.auxintransport_ABCB_ode_noBC import auxintransport_ABCB_ode_noBC

from models.set_ABCB_carriers import set_ABCB_carriers,simple_knockout
from vroot.pylab_plot import plot_pylab_figure_time
import pickle

timestep=20
nsteps=180


for gt in ['wt','a1a19']:


    for i in [1,4]:
        tissuename = "Tissues/DIIV_mock_050517.zip"
        sim=Simulation(tissuename,'VIDrootward_%s_scen%d_adj' % (gt,i),biochemical=auxintransport_ABCB_ode_noBC,output=True)
        sim.set_param_values({'ABCB_scen':i})
        sim.reset_time()
        db=sim.get_db()

        auxin=db.get_property('auxin')
        cell_type=db.get_property('cell_type')
        zones=db.get_property('zones')

        auxin_wall=db.get_property('auxin_wall')

        border=db.get_property('border')

        A1=db.get_property('ABCB1')
        A19=db.get_property('ABCB19')
        A4=db.get_property('ABCB4')
        

                
        if gt=='a1a19':
            for eid,val in A4.items():
                A1[eid]=0.0
                A19[eid]=0.0
                A4[eid]=val*1.299

                
                


        for wid in auxin_wall.keys():
            auxin_wall[wid]=0.001

        for cid,val in auxin.items():
            if cell_type[cid] in [5,16] and cid in border.keys():
                auxin[cid]=1.0
            else:
                auxin[cid]=0.0001

        sim.set_timestep(timestep)
        sim.run_simulation(nsteps)




for gt in ['wt','a4']:


    for i in [1,4]:
        tissuename = "Tissues/DIIV_mock_050517.zip"
        sim=Simulation(tissuename,'VIDshootward_%s_scen%d_adj' % (gt,i),biochemical=auxintransport_ABCB_ode_noBC,output=True)
        sim.set_param_values({'ABCB_scen':i})
        sim.reset_time()
        db=sim.get_db()

        auxin=db.get_property('auxin')
        cell_type=db.get_property('cell_type')
        zones=db.get_property('zones')

        auxin_wall=db.get_property('auxin_wall')

        border=db.get_property('border')
        
        A1=db.get_property('ABCB1')
        A19=db.get_property('ABCB19')
        A4=db.get_property('ABCB4')
                
        if gt=='a4':
            for eid,val in A1.items():
                A4[eid]=0.0
                A1[eid]=val*1.240
                A19[eid]=A19[eid]*1.336

                
        for wid in auxin_wall.keys():
            auxin_wall[wid]=0.001

        
        for cid,val in auxin.items():
            if cell_type[cid] in [6,15] and zones[cid]==0:
                auxin[cid]=1.0
            else:
                auxin[cid]=0.0001
        

        sim.set_timestep(timestep)
        sim.run_simulation(nsteps)





for gt in ['wt','a1a19']:
    for i in [1,4]:
        #pick out db
        fdir='output/DIIV_mock_050517/auxintransport_ABCB_ode_noBC/VIDrootward_%s_scen%d_adj/' % (gt,i)
        
        for j in range(181):
            fname='%siter_%05d.zip' % (fdir,j)
            print(fname)
            db=TissueDB()    
            db.read(fname)
            plot_pylab_figure_time(db,'%sauxin%05d.png' % (fdir,j) ,{'auxin':[0,0.1]},(1,1),True,20*j)

        vidstring="ffmpeg -r 5 -i %sauxin%%05d.png -b:v 1000k output/rootward_%s_%s.mp4" % (fdir,gt,i)
        os.system(vidstring)

for gt in ['wt','a4']:
    for i in [1,4]:
        #pick out db
        fdir='output/DIIV_mock_050517/auxintransport_ABCB_ode_noBC/VIDshootward_%s_scen%d_adj/' % (gt,i)
        
        for j in range(181):
            fname='%siter_%05d.zip' % (fdir,j)
            print(fname)
            db=TissueDB()    
            db.read(fname)
            plot_pylab_figure_time(db,'%sauxin%05d.png' % (fdir,j) ,{'auxin':[0,0.1]},(1,1),True,20*j)

        vidstring="ffmpeg -r 5 -i %sauxin%%05d.png -b:v 1000k output/shootward_%s_%s.mp4" % (fdir,gt,i)
        os.system(vidstring)
