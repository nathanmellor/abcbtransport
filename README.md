Code to produces the figures published in Nathan L Mellor, Ute Voß, Alexander Ware, George Janes, Duncan Barrack, Anthony Bishopp, Malcolm J Bennett, Markus Geisler, Darren M Wells, Leah R Band. 2022, Plant Cell.  'Systems approaches reveal that ABCB and PIN proteins mediate co-dependent auxin efflux'.

For a description of all models, scenarios and parameters see the original paper and supplementary information.

To run output for a particular figure or figures, just run the corresponding script in the top level folder.
For example running 'python Figure2_ABCB_wt_5scenarios.py' will produce model output corresponding to Figure 2 in the paper.

 *** All code requires Python 2.7 rather than Python 3 to run correctly ***

Prerequisites:
 *   numpy
 *   scipy
 *   matplotlib
 *   lxml
 *   xlrd

The exception is Figure5_S14_singlefile_ABCB.m, which requires Matlab (or possibly Octave, though this has not been tested).
