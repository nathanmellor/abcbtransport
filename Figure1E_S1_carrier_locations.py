
from vroot.simulation import Simulation
from vroot.db_utilities import get_graph

from models.set_ABCB_carriers import set_ABCB_carriers
from models.auxintransport_ABCB import auxintransport_ABCB


tissuename = "Tissues/DIIV_mock_050517.zip"



sim=Simulation(tissuename,'carrier_locations',biochemical=auxintransport_ABCB)
set_ABCB_carriers(sim.get_db())


#plot carriers

for PINname in ['PIN1','PIN2','PIN3','PIN4','PIN7']:
    sim.plot_figure(PINname,{PINname:[0,1]},[1,1],True,{1:'g'},[PINname])
    
for ABCBname in ['ABCB1','ABCB19','ABCB4']:
    sim.plot_figure(ABCBname,{ABCBname:[0,1]},[1,1],True,{1:'b'},[ABCBname])
    
sim.plot_figure('AUX1',{'AUX1':[0,1]},[1,1],True,{1:'r'},['AUX1'])


db=sim.get_db()
graph=get_graph(db)
cell_type=db.get_property('cell_type')
LAX=db.get_property('LAX')
LAX2={eid:0.0 for eid in LAX.keys()}

LAX3={eid:0.0 for eid in LAX.keys()}


for eid,val in LAX.items():
    if val==1.0:
        if cell_type[graph.source(eid)]==12:
            LAX3[eid]=1.0
        else:
            LAX2[eid]=1.0


db.set_property('LAX2',LAX2)
db.set_description('LAX2',' ')

db.get_property('species_desc')['LAX2']='edge'


db.set_property('LAX3',LAX3)
db.set_description('LAX3',' ')

db.get_property('species_desc')['LAX3']='edge'



sim.plot_figure('LAX2',{'LAX2':[0,1]},[1,1],True,{1:'y'},['LAX2'])
sim.plot_figure('LAX3',{'LAX3':[0,1]},[1,1],True,{1:'y'},['LAX3'])


toplot={eid:0.0 for eid in LAX.keys()}
for ABCBname in ['ABCB1','ABCB19','ABCB4']:
    for PINname in ['PIN1','PIN2','PIN3','PIN4','PIN7']:
        PIN=db.get_property(PINname)
        ABCB=db.get_property(ABCBname)
        
        
        
        for eid,val in PIN.items():
            if val==1 and ABCB[eid]==1:
                toplot[eid]=1
db.set_property('ABCB_PIN',toplot)
db.set_description('ABCB_PIN',' ')
db.get_property('species_desc')['ABCB_PIN']='edge'






toplot={eid:0.0 for eid in LAX.keys()}
for PINname in ['PIN1','PIN2','PIN3','PIN4','PIN7']:
    

    PIN=db.get_property(PINname)
    for eid,val in PIN.items():
        if val==1:
            noABCB=True
            for ABCBname in ['ABCB1','ABCB19','ABCB4']:
                ABCB=db.get_property(ABCBname)
                if ABCB[eid]==1:
                    noABCB=False
            if noABCB==True:
                toplot[eid]=1.0
                
db.set_property('PIN_noABCB',toplot)
db.set_description('PIN_noABCB',' ')
db.get_property('species_desc')['PIN_noABCB']='edge'



toplot={eid:0.0 for eid in LAX.keys()}

for ABCBname in ['ABCB1','ABCB19','ABCB4']:
    

    ABCB=db.get_property(ABCBname)
    for eid,val in ABCB.items():
        if val==1:
            noPIN=True
            for PINname in ['PIN1','PIN2','PIN3','PIN4','PIN7']:
                PIN=db.get_property(PINname)
                if PIN[eid]==1:
                    noPIN=False
            if noPIN==True:
                toplot[eid]=1.0
db.set_property('ABCB_noPIN',toplot)
db.set_description('ABCB_noPIN',' ')
db.get_property('species_desc')['ABCB_noPIN']='edge'
sim.plot_figure('ABCB_noPIN',{'ABCB_noPIN':[0,1]},[1,1],True,{1:'b'},['ABCB no PIN'])

for eid,val in db.get_property('PIN_noABCB').items():
    if val==1 and db.get_property('ABCB')[eid]==0:
        db.get_property('ABCB_PIN')[eid]=2.0
sim.plot_figure('ABCB_PIN_noABCB_PIN',{'ABCB_PIN':[0,2]},[1,1],True,{1:'b',2:'g'},['ABCB + PIN','PIN only'])


ABCB1=db.get_property('ABCB1')
ABCB4=db.get_property('ABCB4')
ABCB19=db.get_property('ABCB19')

whichABCB={eid:0.0 for eid in ABCB1.keys()}
for eid,val in ABCB1.items():
    if val==1 and ABCB4[eid]==1:
        whichABCB[eid]=2.0
        assert ABCB19[eid]==0
    if val==1 and ABCB19[eid]==1:
        whichABCB[eid]=1.0
        assert ABCB4[eid]==0
    if val==0 and ABCB19[eid]==0 and ABCB4[eid]==1:
        whichABCB[eid]=3.0


db.set_property('whichABCB',whichABCB)
db.set_description('whichABCB',' ')
db.get_property('species_desc')['whichABCB']='edge'

sim.plot_figure('whichABCB',{'whichABCB':[0,3]},[1,1],True,{1:'b',2:'g',3:'r'},['ABCB1 + ABCB19','ABCB1 + ABCB4','ABCB4'])
