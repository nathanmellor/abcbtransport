
from vroot.simulation import Simulation

from models.auxintransport_ABCB import auxintransport_ABCB
from numpy import mean

from models.set_ABCB_carriers import set_ABCB_carriers,simple_knockout
from pylab import *
import matplotlib.pyplot as plt
from scipy.stats import sem



def compare_with_celltypes(db):

    cell_centres=db.get_property('cell_centres')
    cell_type=db.get_property('cell_type')
    VENUS=db.get_property('VENUS')
    DII=db.get_property('nuclei_data')
    zones=db.get_property('zones')
    epi_model_mz=[]
    epi_data_mz=[]
    cor_model_mz=[]
    cor_data_mz=[]
    epi_model_ez=[]
    epi_data_ez=[]
    cor_model_ez=[]
    cor_data_ez=[]
    for cid,val in cell_type.items():
        if val==2:
            if zones[cid]>3:
                epi_model_ez.append(VENUS[cid])
                if DII[cid]>10.0:
                    epi_data_ez.append(DII[cid])
            else:
                epi_model_mz.append(VENUS[cid])
                if DII[cid]>10.0:
                    epi_data_mz.append(DII[cid])
        if val==4:
            if zones[cid]>4:
                cor_model_ez.append(VENUS[cid])
                if DII[cid]>10.0:
                    cor_data_ez.append(DII[cid])
            else:
                cor_model_mz.append(VENUS[cid])
                if DII[cid]>10.0:
                    cor_data_mz.append(DII[cid])

step=0.1
samples=40

cm_to_inch=0.394

for pname in ['PABCB','P_PABCB','PPIN','PAUX1','Pplas','PLAX']:
    for i in range(1,6):
        print('********',i)
        vals={'CEZ':[],'CMZ':[],'EEZ':[],'EMZ':[],'end':[],'stele':[]}
        pvals=[]
        for pval in range(samples+1):
            tissuename = "Tissues/DIIV_mock_271017_s002.zip"
            sim=Simulation(tissuename,'pscan',biochemical=auxintransport_ABCB, output = False)
            db=sim.get_db()
            set_ABCB_carriers(db)
            sim.set_param_values({'ABCB_scen':i,pname:step*pval})
            sim.run_simulation(1)
            
            auxin=db.get_property('auxin')
            cell_type=db.get_property('cell_type')
            zones=db.get_property('zones')
            
            vals['CEZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==4 and zones[cid]==5]))
            vals['CMZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==4 and zones[cid]<5]))

            vals['EEZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==2 and zones[cid]>3]))
            vals['EMZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==2 and zones[cid]<4]))
            
            vals['end'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==3]))
            vals['stele'].append(mean([val for cid,val in auxin.items() if cell_type[cid] in [5,16,25]]))


            pvals.append(pval*step)
        fig, ax1 = plt.subplots(figsize=(8.0*cm_to_inch,6.0*cm_to_inch))

        ax1.plot(pvals,vals['EEZ'],'c',label='Epi EZ')
        ax1.plot(pvals,vals['EMZ'],'b',label='Epi MZ')
        ax1.plot(pvals,vals['CEZ'],'m',label='Cor EZ')
        ax1.plot(pvals,vals['CMZ'],'r',label='Cor MZ')
        ax1.plot(pvals,vals['end'],'k',label='End.')
        ax1.plot(pvals,vals['stele'],'g',label='Stele')

        ax1.set_xlabel(pname,FontSize=10)

        ax1.set_ylabel('auxin',FontSize=10)
        ax1.set_title('scenario %d' % i,FontSize=10)
        #ax1.legend()
        plt.gcf().subplots_adjust(left=0.20,bottom=0.2)
        savefig('output/Auxin_scan_%s_s%d' % (pname,i))
        clf()
        close(fig)


for pname in ['PABCB','P_PABCB','PPIN','PAUX1','Pplas','PLAX']:
    for i in range(1,6):
        print('********',i)
        vals={'CEZ':[],'CMZ':[],'EEZ':[],'EMZ':[],'end':[],'stele':[]}
        pvals=[]
        for pval in range(samples+1):
            tissuename = "Tissues/b1.zip"
            sim=Simulation(tissuename,'pscan',biochemical=auxintransport_ABCB, output = False)
            db=sim.get_db()
            set_ABCB_carriers(db)
            simple_knockout(db,'ABCB1')
            sim.set_param_values({'ABCB_scen':i,pname:step*pval})
            sim.run_simulation(1)
            
            auxin=db.get_property('auxin')
            cell_type=db.get_property('cell_type')
            zones=db.get_property('zones')
            
            vals['CEZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==4 and zones[cid]==5]))
            vals['CMZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==4 and zones[cid]<5]))

            vals['EEZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==2 and zones[cid]>3]))
            vals['EMZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==2 and zones[cid]<4]))
            
            vals['end'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==3]))
            vals['stele'].append(mean([val for cid,val in auxin.items() if cell_type[cid] in [5,16,25]]))


            pvals.append(pval*step)
        fig, ax1 = plt.subplots(figsize=(8.0*cm_to_inch,6.0*cm_to_inch))

        ax1.plot(pvals,vals['EEZ'],'c',label='Epi EZ')
        ax1.plot(pvals,vals['EMZ'],'b',label='Epi MZ')
        ax1.plot(pvals,vals['CEZ'],'m',label='Cor EZ')
        ax1.plot(pvals,vals['CMZ'],'r',label='Cor MZ')
        ax1.plot(pvals,vals['end'],'k',label='End.')
        ax1.plot(pvals,vals['stele'],'g',label='Stele')

        ax1.set_xlabel(pname,FontSize=10)

        ax1.set_ylabel('auxin',FontSize=10)
        ax1.set_title('scenario %d' % i,FontSize=10)
        #ax1.legend()
        plt.gcf().subplots_adjust(left=0.20,bottom=0.2)
        savefig('output/b1_Auxin_scan_%s_s%d' % (pname,i))
        clf()
        close(fig)

for pname in ['PABCB','P_PABCB','PPIN','PAUX1','Pplas','PLAX']:
    for i in range(1,6):
        print('********',i)
        vals={'CEZ':[],'CMZ':[],'EEZ':[],'EMZ':[],'end':[],'stele':[]}
        pvals=[]
        for pval in range(samples+1):
            tissuename = "Tissues/b4.zip"
            sim=Simulation(tissuename,'pscan',biochemical=auxintransport_ABCB, output = False)
            db=sim.get_db()
            set_ABCB_carriers(db)
            simple_knockout(db,'ABCB4')
            sim.set_param_values({'ABCB_scen':i,pname:step*pval})
            sim.run_simulation(1)
            
            auxin=db.get_property('auxin')
            cell_type=db.get_property('cell_type')
            zones=db.get_property('zones')
            
            vals['CEZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==4 and zones[cid]==5]))
            vals['CMZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==4 and zones[cid]<5]))

            vals['EEZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==2 and zones[cid]>3]))
            vals['EMZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==2 and zones[cid]<4]))
            
            vals['end'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==3]))
            vals['stele'].append(mean([val for cid,val in auxin.items() if cell_type[cid] in [5,16,25]]))


            pvals.append(pval*step)
        fig, ax1 = plt.subplots(figsize=(8.0*cm_to_inch,6.0*cm_to_inch))

        ax1.plot(pvals,vals['EEZ'],'c',label='Epi EZ')
        ax1.plot(pvals,vals['EMZ'],'b',label='Epi MZ')
        ax1.plot(pvals,vals['CEZ'],'m',label='Cor EZ')
        ax1.plot(pvals,vals['CMZ'],'r',label='Cor MZ')
        ax1.plot(pvals,vals['end'],'k',label='End.')
        ax1.plot(pvals,vals['stele'],'g',label='Stele')

        ax1.set_xlabel(pname,FontSize=10)

        ax1.set_ylabel('auxin',FontSize=10)
        ax1.set_title('scenario %d' % i,FontSize=10)
        #ax1.legend()
        plt.gcf().subplots_adjust(left=0.20,bottom=0.2)
        savefig('output/b4_Auxin_scan_%s_s%d' % (pname,i))
        clf()
        close(fig)
        
for pname in ['PABCB','P_PABCB','PPIN','PAUX1','Pplas','PLAX']:
    for i in range(1,6):
        print('********',i)
        vals={'CEZ':[],'CMZ':[],'EEZ':[],'EMZ':[],'end':[],'stele':[]}
        pvals=[]
        for pval in range(samples+1):
            tissuename = "Tissues/b19.zip"
            sim=Simulation(tissuename,'pscan',biochemical=auxintransport_ABCB, output = False)
            db=sim.get_db()
            set_ABCB_carriers(db)
            simple_knockout(db,'ABCB19')
            sim.set_param_values({'ABCB_scen':i,pname:step*pval})
            sim.run_simulation(1)
            
            auxin=db.get_property('auxin')
            cell_type=db.get_property('cell_type')
            zones=db.get_property('zones')
            
            vals['CEZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==4 and zones[cid]==5]))
            vals['CMZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==4 and zones[cid]<5]))

            vals['EEZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==2 and zones[cid]>3]))
            vals['EMZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==2 and zones[cid]<4]))
            
            vals['end'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==3]))
            vals['stele'].append(mean([val for cid,val in auxin.items() if cell_type[cid] in [5,16,25]]))


            pvals.append(pval*step)
        fig, ax1 = plt.subplots(figsize=(8.0*cm_to_inch,6.0*cm_to_inch))

        ax1.plot(pvals,vals['EEZ'],'c',label='Epi EZ')
        ax1.plot(pvals,vals['EMZ'],'b',label='Epi MZ')
        ax1.plot(pvals,vals['CEZ'],'m',label='Cor EZ')
        ax1.plot(pvals,vals['CMZ'],'r',label='Cor MZ')
        ax1.plot(pvals,vals['end'],'k',label='End.')
        ax1.plot(pvals,vals['stele'],'g',label='Stele')

        ax1.set_xlabel(pname,FontSize=10)

        ax1.set_ylabel('auxin',FontSize=10)
        ax1.set_title('scenario %d' % i,FontSize=10)
        #ax1.legend()
        plt.gcf().subplots_adjust(left=0.20,bottom=0.2)
        savefig('output/b19_Auxin_scan_%s_s%d' % (pname,i))
        clf()
        close(fig)
        
        
for pname in ['PABCB','P_PABCB','PPIN','PAUX1','Pplas','PLAX']:
    for i in range(1,6):
        print('********',i)
        vals={'CEZ':[],'CMZ':[],'EEZ':[],'EMZ':[],'end':[],'stele':[]}
        pvals=[]
        for pval in range(samples+1):
            tissuename = "Tissues/b4-1b19-1_april2019_rep1.zip"
            sim=Simulation(tissuename,'pscan',biochemical=auxintransport_ABCB, output = False)
            db=sim.get_db()
            set_ABCB_carriers(db)
            simple_knockout(db,'ABCB4')
            simple_knockout(db,'ABCB19')
            sim.set_param_values({'ABCB_scen':i,pname:step*pval})
            sim.run_simulation(1)
            
            auxin=db.get_property('auxin')
            cell_type=db.get_property('cell_type')
            zones=db.get_property('zones')
            
            vals['CEZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==4 and zones[cid]==5]))
            vals['CMZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==4 and zones[cid]<5]))

            vals['EEZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==2 and zones[cid]>3]))
            vals['EMZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==2 and zones[cid]<4]))
            
            vals['end'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==3]))
            vals['stele'].append(mean([val for cid,val in auxin.items() if cell_type[cid] in [5,16,25]]))


            pvals.append(pval*step)
        fig, ax1 = plt.subplots(figsize=(8.0*cm_to_inch,6.0*cm_to_inch))

        ax1.plot(pvals,vals['EEZ'],'c',label='Epi EZ')
        ax1.plot(pvals,vals['EMZ'],'b',label='Epi MZ')
        ax1.plot(pvals,vals['CEZ'],'m',label='Cor EZ')
        ax1.plot(pvals,vals['CMZ'],'r',label='Cor MZ')
        ax1.plot(pvals,vals['end'],'k',label='End.')
        ax1.plot(pvals,vals['stele'],'g',label='Stele')

        ax1.set_xlabel(pname,FontSize=10)

        ax1.set_ylabel('auxin',FontSize=10)
        ax1.set_title('scenario %d' % i,FontSize=10)
        #ax1.legend()
        plt.gcf().subplots_adjust(left=0.20,bottom=0.2)
        savefig('output/b4b19_Auxin_scan_%s_s%d' % (pname,i))
        clf()
        close(fig)

for pname in ['PABCB','P_PABCB','PPIN','PAUX1','Pplas','PLAX']:
    for i in range(1,6):
        print('********',i)
        vals={'CEZ':[],'CMZ':[],'EEZ':[],'EMZ':[],'end':[],'stele':[]}
        pvals=[]
        for pval in range(samples+1):
            tissuename = "Tissues/b4-1b1-100_april2019_rep2.zip"
            sim=Simulation(tissuename,'pscan',biochemical=auxintransport_ABCB, output = False)
            db=sim.get_db()
            set_ABCB_carriers(db)
            simple_knockout(db,'ABCB1')
            simple_knockout(db,'ABCB4')
            
            sim.set_param_values({'ABCB_scen':i,pname:step*pval})
            sim.run_simulation(1)
            
            auxin=db.get_property('auxin')
            cell_type=db.get_property('cell_type')
            zones=db.get_property('zones')
            
            vals['CEZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==4 and zones[cid]==5]))
            vals['CMZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==4 and zones[cid]<5]))

            vals['EEZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==2 and zones[cid]>3]))
            vals['EMZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==2 and zones[cid]<4]))
            
            vals['end'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==3]))
            vals['stele'].append(mean([val for cid,val in auxin.items() if cell_type[cid] in [5,16,25]]))


            pvals.append(pval*step)
        fig, ax1 = plt.subplots(figsize=(8.0*cm_to_inch,6.0*cm_to_inch))

        ax1.plot(pvals,vals['EEZ'],'c',label='Epi EZ')
        ax1.plot(pvals,vals['EMZ'],'b',label='Epi MZ')
        ax1.plot(pvals,vals['CEZ'],'m',label='Cor EZ')
        ax1.plot(pvals,vals['CMZ'],'r',label='Cor MZ')
        ax1.plot(pvals,vals['end'],'k',label='End.')
        ax1.plot(pvals,vals['stele'],'g',label='Stele')

        ax1.set_xlabel(pname,FontSize=10)

        ax1.set_ylabel('auxin',FontSize=10)
        ax1.set_title('scenario %d' % i,FontSize=10)
        #ax1.legend()
        plt.gcf().subplots_adjust(left=0.20,bottom=0.2)
        savefig('output/b1b4_Auxin_scan_%s_s%d' % (pname,i))
        clf()
        close(fig)

for pname in ['PABCB','P_PABCB','PPIN','PAUX1','Pplas','PLAX']:
    for i in range(1,6):
        print('********',i)
        vals={'CEZ':[],'CMZ':[],'EEZ':[],'EMZ':[],'end':[],'stele':[]}
        pvals=[]
        for pval in range(samples+1):
            tissuename = "Tissues/b1-100b19-1_april2019_rep1.zip"
            sim=Simulation(tissuename,'pscan',biochemical=auxintransport_ABCB, output = False)
            db=sim.get_db()
            set_ABCB_carriers(db)
            simple_knockout(db,'ABCB1')
            simple_knockout(db,'ABCB19')
            
            sim.set_param_values({'ABCB_scen':i,pname:step*pval})
            sim.run_simulation(1)
            
            auxin=db.get_property('auxin')
            cell_type=db.get_property('cell_type')
            zones=db.get_property('zones')
            
            vals['CEZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==4 and zones[cid]==5]))
            vals['CMZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==4 and zones[cid]<5]))

            vals['EEZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==2 and zones[cid]>3]))
            vals['EMZ'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==2 and zones[cid]<4]))
            
            vals['end'].append(mean([val for cid,val in auxin.items() if cell_type[cid]==3]))
            vals['stele'].append(mean([val for cid,val in auxin.items() if cell_type[cid] in [5,16,25]]))


            pvals.append(pval*step)
        fig, ax1 = plt.subplots(figsize=(8.0*cm_to_inch,6.0*cm_to_inch))

        ax1.plot(pvals,vals['EEZ'],'c',label='Epi EZ')
        ax1.plot(pvals,vals['EMZ'],'b',label='Epi MZ')
        ax1.plot(pvals,vals['CEZ'],'m',label='Cor EZ')
        ax1.plot(pvals,vals['CMZ'],'r',label='Cor MZ')
        ax1.plot(pvals,vals['end'],'k',label='End.')
        ax1.plot(pvals,vals['stele'],'g',label='Stele')

        ax1.set_xlabel(pname,FontSize=10)

        ax1.set_ylabel('auxin',FontSize=10)
        ax1.set_title('scenario %d' % i,FontSize=10)
        #ax1.legend()
        plt.gcf().subplots_adjust(left=0.20,bottom=0.2)
        savefig('output/b1b19_Auxin_scan_%s_s%d' % (pname,i))
        clf()
        close(fig)
