# general framework used to run biochemical models in a tissue db tissue structure

from vroot.db_utilities import refresh_property, get_wall_decomp, get_tissue_maps,set_parameters,get_parameters,get_graph,get_mesh,def_property,updateVS


from openalea.tissueshape import edge_length, face_surface_2D

try:
    from odesparse import odeints
except:
    from scipy.integrate import odeint
    
from scipy.sparse import lil_matrix
from numpy import zeros, concatenate



class CombinedModel(object):
    """
    Additive combination of GRN models
    """

    name = 'CombinedModel'

    def __init__(self, models):
        """
        :param models: List of models to combine (each implementing
                       the AbstractModel interface)
        """
        self.models=models

    def set_default_parameters(self, db):
        for model in self.models:
            model.set_default_parameters(db)

    def get_diluted_props(self):
        return set.union(*[set(m.diluted_props) for m in self.models])


    def get_species(self):
        """
        :returns: Set of names of species in the combined model
        """
        return set.union(*[set(m.get_species()) for m in self.models])

    def get_steady_species(self):
        return set.union(*[set(m.get_steady_species()) for m in self.models])

    def get_model_names(self):
        names = []
        for model in self.models:
            names.append(model.__class__.__name__)
        return names
    
    def deriv(self, y, t, *args):
        """
        Derivative function - sum of the derivatives for 
        each component model
        :param y: Current state vector
        :type y: numpy.array (or list)
        :param t: Current time
        :type t: float
        :param *args: additional arguments for derivative function
        """
        return sum((m.deriv(y, t, *args) for m in self.models),zeros(y.shape))



    def set_ss(self, db, tissue_maps, wall_decomposition, species_slices):
        for model in self.models:
            model.set_ss(db, tissue_maps, wall_decomposition, species_slices)


class Biochemical(object):
    """
    Class to handle integration of GRN and cell-cell transport
    """
    def __init__(self, db, model):
        """ 
        Initialise the networks 
        :param db: Tissue database
        :type db: TissueDB
        :param model: implementation of AbstractModel
        """
        self.db = db
        # Construct a model for each of the model classes
        self.model=model
        self.model_species=model.get_species()

        self.steady_species=model.get_steady_species()

        species_desc=self.db.get_property('species_desc')


        # Initialise all of the species needed which are not already
        # contained in the TissueDB
        for species_name, species_type  in self.model_species:
            species_desc[species_name]=species_type
            if species_name not in db.properties():
                self.db,prop = def_property(db,species_name,0.0,species_type.upper(), "config","")


        for species_name, species_type  in self.steady_species:
            species_desc[species_name]=species_type
            if species_name not in db.properties():
                self.db,prop = def_property(db, species_name, 0.0,species_type.upper(),"config","")

        model.set_default_parameters(db)

        set_fixed_properties(db)
    
        self.update_tissue()

    def update_tissue(self):
        """
        update various geometric properties of the tissue
        """
        self.wall_decomposition = get_wall_decomp(self.db)
        updateVS(self.db)
        self.tissue_maps = get_tissue_maps(self.db)
        self.offset=0 # current offset into the y[] array
        self.species_slices={} # map from species name to the slice of y[]
        for species_name, species_type in self.model_species:
            # 'size' is how many values are needed to store a property
            # of that type 
            size = len(self.tissue_maps[species_type])
            self.species_slices[species_name] = slice(self.offset, self.offset+size)
            self.offset+=size
        self.Jpat=self.get_Jpat(self.species_slices,self.offset,self.db,self.model)
    
    def step(self):
        """
        Evolve the gene network for one timestep
        """

        #set fixed properties, may use updated dictionaries from previous step
        set_fixed_properties(self.db)   
   
        # Call routine to find fixed property values
        fixed_idx,fixed_steady=self.get_fixed_idx(self.tissue_maps, self.species_slices)

        # assign steady state values before integration step
        self.model.set_ss(self.db, self.tissue_maps, fixed_idx, self.species_slices)

        #initialise with zero
        y0=zeros((self.offset,))

        ### SETUP INITIAL VALUES
        # Loop over all species referred to in these models
        for species_name, species_type in self.model_species:
            # extract property dict from TissueDB
            prop=self.db.get_property(species_name)
            # obtain appropriate section of y0[]
            y0_slice=y0[self.species_slices[species_name]]
            for tid, idx in self.tissue_maps[species_type].items() :
                y0_slice[idx]=prop[tid]

        ###INTEGRATE OVER TIMESTEP
        dt=get_parameters(self.db, 'timestep')

        try:
            integrated = odeints(self.model.deriv, y0, [0., dt], (self.db, self.tissue_maps, self.species_slices,self.wall_decomposition, fixed_idx)\
                    ,rtol=1e-16,lrw=1000000,JPat=self.Jpat)
        except:
            integrated = odeint(self.model.deriv, y0, [0., dt], args= (self.db, self.tissue_maps, self.species_slices,self.wall_decomposition, fixed_idx))

        ###SET SPECIES TO NEW VALUE (AFTER TIMESTEP)
        yend=integrated[-1,:]
        
        for species_name, species_type in self.model_species: 
            prop=self.db.get_property(species_name)
            yend_slice=yend[self.species_slices[species_name]]
            for tid, idx in self.tissue_maps[species_type].items() :
                prop[tid]=yend_slice[idx]

        mesh=get_mesh(self.db)
        iwalls=[]
        for wid in mesh.wisps(1):
            if len(list(mesh.regions(1,wid)))==2:
                iwalls.append(self.tissue_maps['wall'][wid])



    def get_fixed_idx(self, tissue_maps, species_slices):
        """
        Obtain a list of those elements in the ODE state vector y
        which are constant during the simulation
        (From the property 'fixed' in the tissue database.)
        This currently only works for cell-based properties
        
        The dictionary fixed maps a property name to a list
        of (expression, value) rules; for each cell, if the expression
        is satisfied, then the property is fixed
        This is a Python expression, and may depend on the cell
        id number and the values of all the properties of the tissue.

        e.g. fixed = { 'auxin':[ ('cell_type[cid]=='pericycle', 2) ] }
        would fix the auxin concentration in the pericycle.

        :param tissue_maps:
        :param species_slices:
        :returns: list of the offsets into the state vector y[] which
                  are to be held constant.

        """
        fixed = get_parameters(self.db, 'fixed')
        
        cell_map = tissue_maps['cell']
        edge_map = tissue_maps['edge']
        wall_map = tissue_maps['wall']
        fixed_idx = [] # list of fixed indices
        fixed_steady={} # dist of steady props and cids
        # Extract all the properties from the tissue data,
        # so these can be used in the eval statement
        all_properties=dict((name, self.db.get_property(name)) \
                                for name in self.db.properties())
        # Loop over all properties with rules

        for prop_name, rules in fixed.items():      
            # Ignore properties not in model
            if prop_name in species_slices:     
                s=species_slices[prop_name]
                prop=self.db.get_property(prop_name)
                for (expr, value) in rules:
                    # Parse expression and convert it to python
                    # bytecode
                    code=compile(expr, '', 'eval')
                    if dict(self.model_species)[prop_name]=='cell':
                            # Loop over all cells
                        for cid, i in cell_map.items():
                                # Test the expression for this cid
                                if eval(code, all_properties, { 'cid':cid }):
                                    #check cid 
                                    if cid in prop.keys():
                                        idx=s.start+i
                                        fixed_idx.append(idx)
                    if dict(self.model_species)[prop_name]=='wall':
                            # Loop over all walls
                        for cid, i in wall_map.items():
                                # Test the expression for this cid
                                if eval(code, all_properties, { 'cid':cid }):
                                    #check cid
                                    if cid in prop.keys():
                                        idx=s.start+i
                                        fixed_idx.append(idx)
                    if dict(self.model_species)[prop_name]=='edge':
                            # Loop over all walls
                        for cid, i in edge_map.items():
                                # Test the expression for this cid
                                if eval(code, all_properties, { 'cid':cid }):
                                    #check cid
                                    if cid in prop.keys():
                                        idx=s.start+i
                                        fixed_idx.append(idx)
        
            elif prop_name in dict(self.steady_species):
                prop=self.db.get_property(prop_name)
                fixed_steady[prop_name]=[]
                for (expr, value) in rules:
                    # Parse expression and convert it to python
                    # bytecode
                    code=compile(expr, '', 'eval')
                    if dict(self.steady_species)[prop_name]=='cell':
                            # Loop over all cells
                        for cid, i in cell_map.items():
                                # Test the expression for this cid
                                if eval(code, all_properties, { 'cid':cid }):
                                    #check cid 
                                    if cid in prop.keys():
                                        fixed_steady[prop_name].append(cid)
                    if dict(self.steady_species)[prop_name]=='wall':
                            # Loop over all walls
                        for cid, i in wall_map.items():
                                # Test the expression for this cid
                                if eval(code, all_properties, { 'cid':cid }):
                                    #check cid
                                    if cid in prop.keys():
                                        fixed_steady[prop_name].append(cid)
                    if dict(self.steady_species)[prop_name]=='edge':
                            # Loop over all walls
                        for cid, i in edge_map.items():
                                # Test the expression for this cid
                                if eval(code, all_properties, { 'cid':cid }):
                                    #check cid
                                    if cid in prop.keys():
                                        fixed_steady[prop_name].append(cid)
        return fixed_idx,fixed_steady

    def get_Jpat(self,species_slices,offset,db,model):
        JPat=lil_matrix((offset,offset))
        tissue_maps = get_tissue_maps(db)
        cell_map = tissue_maps['cell']
        wall_map = tissue_maps['wall']
        graph=get_graph(db)
        mesh=get_mesh(db)
        wall_deg=mesh.degree()-1
        wall_decomposition=get_wall_decomp(db)
        
        #TODO: auxin is hardcoded - if there is another mobile species
        #will have to update jacobian
        csl = species_slices['auxin']
        wsl = species_slices['auxin_wall']

        for wid in mesh.wisps(1):
            if len(list(mesh.regions(1,wid)))==2:
                (eid1,eid2)=wall_decomposition[wid]
                cell1 = graph.source(eid1)
                cell2 = graph.target(eid1)
                index_cell1 = csl.start + cell_map[cell1]
                index_cell2 = csl.start + cell_map[cell2]
                index_wall= wsl.start + wall_map[wid]
                JPat[index_cell1,index_wall]=1.0
                JPat[index_wall,index_cell1]=1.0
                JPat[index_cell2,index_wall]=1.0
                JPat[index_wall,index_cell2]=1.0
                       #update JPat to incorperate inter-auxi diffusion
                JPat[index_cell1,index_cell2]=1.0
                JPat[index_cell2,index_cell1]=1.0
            else:
                (cidlist)=tuple(mesh.regions(1,wid))
                cid=cidlist[0]  
                index_cell = csl.start+cell_map[cid]
                index_wall= wsl.start+wall_map[wid]
                JPat[index_cell,index_wall]=1.0
                JPat[index_wall,index_cell]=1.0
                
        #wall diffusion
        try:
        # version with ODE points
            point_map=tissue_maps['point']
            psl=species_slices['auxin_point']
            for pid in mesh.wisps(0):
                pidx=psl.start+point_map[pid]
                for wid in mesh.regions(0,pid):
                    widx=wsl.start+wall_map[wid]
                    JPat[pidx,widx]=1.0
                    JPat[widx,pidx]=1.0

        
        except KeyError:
            # original
            for pid in mesh.wisps(0):
                idx = [wsl.start+wall_map[wid] for wid in mesh.regions(0,pid)]
                for i in idx:
                    JPat[idx,i]=1.0
        
        
        for cid in mesh.wisps(2):
            idx=csl.start+cell_map[cid]
            JPat[idx,idx]=1.0

        return JPat




def set_fixed_properties(db):
    """
    The dictionary fixed maps a property name to a list
    of (expression, value) rules; for each cell, if the expression
    is satisfied, then the property is set to the value.
    This is a Python expression, and may depend on the cell
    id number and the values of all the properties of the tissue.
    If multiple expressions are satisfied, then all of these
    are applied, so the last in the list is used.

    e.g. fixed = { 'auxin':[ ('cell_type[cid]=='pericycle', 2) ] }
    would set the auxin concentration in the pericycle to 2.

    :param db:  tissue database
    :type db: TissueDB
    """
    # Extract mesh from TissueDB

    mesh = get_mesh(db)
    graph = get_graph(db)
    # Extract all the properties from the tissue data,
    # so these can be used in the eval statement
    all_properties=dict((name, db.get_property(name)) \
                            for name in db.properties())
    # Loop over all properties with rules
    for prop_name, rules in get_parameters(db, 'fixed').items():

        # Check if property currently in tissue database
        if prop_name in db.properties():
            prop = db.get_property(prop_name)
            for (expr, value) in rules:
                # compile expression to python bytecode
                code=compile(expr, '', 'eval')

                # loop over all cells
        try:
            for cid in mesh.wisps(mesh.degree()):
                # test whether expr is satisfied for this cell
                if eval(code, all_properties, {'cid':cid}):
                #check cid is already a key of property
                    if cid in prop.keys():
                        prop[cid] = value

        #if rule does not apply to cells pass
        except KeyError:
            pass
        try:
            for cid in mesh.wisps(mesh.degree()-1):
                # test whether expr is satisfied for this wall
                if eval(code, all_properties, {'cid':cid}):
                #check cid is already a key of property
                    if cid in prop.keys():
                        prop[cid] = value
        #if rule does not apply to walls pass
        except KeyError:
            pass
        try:
            for cid in graph.edges():
                # test whether expr is satisfied for this edge
                if eval(code, all_properties, {'cid':cid}):
                #check cid is already a key of property
                    if cid in prop.keys():
                        prop[cid] = value
        #if rule does not apply to walls pass           
        except KeyError:
            pass

def model_setup(model,db):

    set_parameters(db, model.__class__.__name__, model.p)
    model.p = get_parameters(db, model.__class__.__name__)
    set_parameters(db, 'fixed', model.fixed)

    model.inner_walls=[]
    model.outer_walls={}
    mesh=get_mesh(db)
    for wid in mesh.wisps(1):
        wcids=list(mesh.regions(1,wid))
        if len(wcids)==2:
            model.inner_walls.append(wid)
        else:
            model.outer_walls[wid]=wcids[0]
    model.points_to_walls={pid:list(mesh.regions(0, pid)) for pid in mesh.wisps(0)}
    

