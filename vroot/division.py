# simple functions for dividing a 2D cell geometry along a given axis
#TODO: combine with 3d division code that also deals with tissue db property code

from vroot.db_utilities import get_mesh,get_graph,get_wall_decomp,centroid
from openalea.container import ordered_pids
    
    
    
def is_between(pt,p0,p1):
    xvals=sorted([p0[0],p1[0]])
    yvals=sorted([p0[1],p1[1]])
    #check test point is between the other two 
    if xvals[0]<=pt[0]<=xvals[1] and yvals[0]<=pt[1]<=yvals[1]:
        #this is just the sufficient condition that the cross product between two lines is zero i.e. they are colinear
        return (p1[0]-p0[0])*(pt[1]-p0[1])==(pt[0]-p0[0])*(p1[1]-p0[1])
    else:
        return False

def wall_from_point(db,pt):
    mesh=get_mesh(db)
    position=db.get_property('position')
    for wid in mesh.wisps(1):
        pids=list(mesh.borders(1,wid))
        if is_between(pt,position[pids[0]],position[pids[1]]):
            return wid
    return None
    
def wall_between_pids(db,p1,p2):
    mesh=get_mesh(db)
    for wid in mesh.regions(0,p1):
        if wid in mesh.regions(0,p2):
            return wid
    return None

#pt must lie along given wall segment
def divide_wall_2d(db,xid,pt):

    mesh=get_mesh(db)
    graph=get_graph(db)
    position=db.get_property('position')
    wall_decomp=get_wall_decomp(db)
    wall=db.get_property('wall')

    
    #get existing ids
    pids=list(mesh.borders(1,xid))
    cids=list(mesh.regions(1,xid))
    if xid in wall_decomp:
        eids=wall_decomp[xid]
    else:
        eids=[]
    
    #check pt is within wall
    assert is_between(pt,position[pids[0]],position[pids[1]])
    
    #remove old wall
    mesh.remove_wisp(1,xid)

    #add point
    npid=mesh.add_wisp(0)
    position[npid]=pt
    
    #add new walls
    wids=[]
    for pid in pids:
        #add new wall
        wids.append(mesh.add_wisp(1))
        #link new wall with cell(s)
        for cid in cids:
            mesh.link(2,cid,wids[-1])
        #link new point with new wall
        mesh.link(1,wids[-1],npid)
        #link old pt with new wall
        mesh.link(1,wids[-1],pid)


    #remove any old edges
    for eid in eids:
        graph.remove_edge(eid)
        del wall[eid]
    
    #add new edges
    if len(cids)>1:
        for wid in wids:
            eid=graph.add_edge(cids[0],cids[1])
            wall[eid]=wid
            eid=graph.add_edge(cids[1],cids[0])
            wall[eid]=wid

    return wids

#two pts must be either be existing points or lie on existing wall
#assume 'limited' concavity i.e. points make line entirely within one cell
def divide_cell_2d(db,cid,pts):
    mesh=get_mesh(db)
    graph=get_graph(db)
    position=db.get_property('position')
    wall=db.get_property('wall')
    pids=ordered_pids(mesh,cid)
    cell_pts=[position[pid] for pid in pids]
    
    for pt in pts:
        if pt not in cell_pts:
            wid = wall_from_point(db,pt)
            nwids=divide_wall_2d(db,wid,pt)
    
    #get pids delineating division
    pids=ordered_pids(mesh,cid)
    xpids=[]
    for pid in pids:
        if position[pid] in pts:
            xpids.append(pid)

    #remove old cell (and edges from wall)
    for eid in graph.out_edges(cid):
        del wall[eid]
    for eid in graph.in_edges(cid):
        del wall[eid]
    mesh.remove_wisp(2,cid)
    
    #add new wall
    nwid=mesh.add_wisp(1)
    #link new wall with pids
    for pid in xpids:
        mesh.link(1,nwid,pid)
    #add new cells
    c0=mesh.add_wisp(2)
    mesh.link(2,c0,nwid)
    c1=mesh.add_wisp(2)
    mesh.link(2,c1,nwid)
    
    #link with old walls
    npts=len(pids)
    pindices=sorted([pids.index(pid) for pid in xpids])
    for i in range(pindices[0],pindices[1]):
        mesh.link(2,c0,wall_between_pids(db,pids[i%npts],pids[(i+1)%npts]))
    for i in range(pindices[1],npts+pindices[0]):
        mesh.link(2,c1,wall_between_pids(db,pids[i%npts],pids[(i+1)%npts]))
        

    #add new edges
    eid=graph.add_edge(c0,c1)
    wall[eid]=nwid
    eid=graph.add_edge(c1,c0)
    wall[eid]=nwid
    set_cell_centres(db)

    for xcid in [c0,c1]:
        for xwid in mesh.borders(2,xcid):
            ncids=list(mesh.regions(1,xwid))
            if xwid!=nwid and len(ncids)==2:
                eid=graph.add_edge(ncids[0],ncids[1])
                wall[eid]=xwid
                eid=graph.add_edge(ncids[1],ncids[0])
                wall[eid]=xwid

    return [c0,c1]
    
#adds a single cell with outline points 'posref' in empty mesh 'mesh' 
def make_outline(db,posref):
    mesh=get_mesh(db)
    position={}
    cid=mesh.add_wisp(2)
    wids=[]
    pids=[]
    for i in range(len(posref)):
        pids.append(mesh.add_wisp(0))
        position[pids[-1]]=posref[i]
        wids.append(mesh.add_wisp(1))
        mesh.link(2,cid,wids[-1])
        mesh.link(1,wids[-1],pids[-1])
    for i in range(len(posref)):
        mesh.link(1,wids[i],pids[i-1])
        
    db.set_property('position',position)
    db.set_description('position','position')
    
    set_cell_centres(db)
    
    return position,cid

def set_cell_centres(db):
    mesh=get_mesh(db)
    position=db.get_property('position')
    cell_centres={}
    for cid in mesh.wisps(2):
        cell_centres[cid]=centroid([position[pid] for pid in ordered_pids(mesh,cid)])
    db.set_property('cell_centres',cell_centres)
    db.set_description('cell_centres','cell centres')
    return cell_centres
