
from vroot.simulation import Simulation

from models.auxintransport_ABCB4_influx import auxintransport_ABCB4_influx

from models.set_ABCB_carriers import set_ABCB_carriers





for i in range(1,6):

    tissuename = "Tissues/DIIV_mock_271017_s002.zip"

    sim=Simulation(tissuename,'wt_scenario_%d' % i,biochemical=auxintransport_ABCB4_influx, output = False)
    db=sim.get_db()
    set_ABCB_carriers(db)
    sim.set_param_values({'ABCB_scen':i})
    sim.run_simulation(1)
    sim.plot_figure('auxin',{'auxin':[0,12.0]},(1,1))
    sim.plot_figure('VENUS',{'VENUS':[0,sim.get_95pc('VENUS')]},(1,1))
    sim.plot_flux('combined_flux','combined_flux',0.5,0.5,'b')



for i in range(1,6):

    tissuename = "Tissues/aux1_DIIV_6_22_mock_271017_s005.zip"
    sim=Simulation(tissuename,'aux1_scenario_%d' % i,biochemical=auxintransport_ABCB4_influx, output = False)
    db=sim.get_db()
    set_ABCB_carriers(db)
    aux1=db.get_property('AUX1')
    for eid in aux1.keys():
        aux1[eid]=0.0
    sim.set_param_values({'ABCB_scen':i})
    sim.run_simulation(1)
    sim.plot_figure('auxin',{'auxin':[0,12.0]},(1,1))
    sim.plot_figure('VENUS',{'VENUS':[0,sim.get_95pc('VENUS')]},(1,1))
    sim.plot_flux('combined_flux','combined_flux',0.5,0.5,'b')

