from numpy import zeros
from math import exp
from vroot.db_utilities import get_mesh, get_graph, get_parameters, set_parameters, get_wall_decomp, get_tissue_maps,def_property
from numpy import size, sqrt,pi
from models.set_plasmodesmata import set_plasmodesmata
from vroot.biochemical import model_setup

class auxintransport_ABCB_ode_noBC(object):
    """

    Implements AbstractModel
    """
    jacobian_links = {'cells_to_walls':[('auxin','auxin_wall')],'cells_to_cells':[],'intracellular':{}}

    diluted_props = ['auxin','auxin_wall']

    def get_species(self):
        return [('VENUS','cell'),('auxin', 'cell'),('auxin_wall','wall')]

    def get_steady_species(self):
        return [('PIN','edge'),('AUX1','edge'),('LAX','edge'),('plas_flux','wall'),('carrier_flux','wall'),('combined_flux','wall'),
                ('auxlax_flux','wall'),('pin_flux','wall'),('passive_flux','wall'),('ABCB','edge'),
                ('ABCB1','edge'),('ABCB4','edge'),('ABCB19','edge')]

    def set_default_parameters(self, db):
        p={}

        # Parameters governing auxin transport
        p['pHapo']=5.3
        p['pHcyt']=7.2
        p['pK']=4.8
        p['Voltage']=-0.120
        p['Temp']=300
        p['Rconst']=8.31
        p['Fdconst']=96500
        p['phi']=p['Fdconst']*p['Voltage']/(p['Rconst']*p['Temp'])
        # Parameters governing auxin transport
        p['A1']=1/(1+pow(10,p['pHapo']-p['pK'])) # A1, A2, A3, B1, B2, B3 are dimensionless
        p['A2']=(1-p['A1'])*p['phi']/(exp(p['phi'])-1)
        p['A3']=-(1-p['A1'])*p['phi']/(exp(-p['phi'])-1)
        p['A4']=-(1-p['A1'])*p['phi']/(exp(-p['phi'])-1)
        p['A5']=-(1-p['A1'])*p['phi']/(exp(-p['phi'])-1)
        
        p['B1']=1/(1+pow(10,p['pHcyt']-p['pK']))
        p['B2']=-(1-p['B1'])*p['phi']/(exp(-p['phi'])-1)
        p['B3']=(1-p['B1'])*p['phi']/(exp(p['phi'])-1)
        p['B4']=(1-p['B1'])*p['phi']/(exp(p['phi'])-1)
        p['B5']=(1-p['B1'])*p['phi']/(exp(p['phi'])-1)

        p['D_cw']= 32.0    # Diffusion rate in cell wall (microns^2 sec^(-1)) from Kramer 2007.


        # The permeability coefficients 
        p['PIAAH']=  0.56 # Diffusion coefficient of IAAH (micrometers.sec-1)
        p['PAUX1']=0.56 # Permeability of anionic auxin due to AUX1 active influx carriers (for an AUX1 concentration equal to 1). (micrometers.sec-1)
        p['PLAX']= 0.56 # Permeability of anionic auxin due to LAX active influx carriers (for an LAX concentration equal to 1). (micrometers.sec-1)
        p['PPIN']=  0.56  # Permeability of anionic auxin due to active efflux carriers (for a PIN concentration equal to 1).   (micrometers.sec-1)    
        p['Pback']=0.0 # Background efflux permeability - replaced by ABCBs in this case
        p['Pplas'] = 8.0/9.92 # Permeability of Plasmodesmata (micrometers.sec-1) 8 in stele (Rustchow et al, 2011) / Plasmodesmata density in transvere wall of Stele
        
        p['PABCB']=  0.56  # Permeability of anionic auxin due to active efflux carriers (for an ABCB concentration equal to 1).   (micrometers.sec-1)  
        p['P_PABCB']=  0.56  # Permeability of anionic auxin due to active efflux of PIN and ABCB working in unison   (micrometers.sec-1)
        p['ABCB_scen'] = 1  

        # Parameters governing DII-VENUS degradation (estimated in Band et al. PNAS (2012)). 
        p['p1']=0.06   #  parameter [Auxin-TIR1-VENUS]u/[TIR1]T
        p['p2']= 0.0053/60 # parameter delta/[VENUS]u - too slow????
        p['q1']=0.91   #  parameter [TIR1]u/[TIR1]T
        p['q2']=0.03   #  parameter [Auxin-TIR1]u/[TIR1]T



        # Auxin production and degradation: 
        p['dauxin']=0.0
        p['pauxin']=('cell_type',{17:0.00,10:0.00,6:0.00,7:0.00,'else':0.000})
        
        #exogenous auxin
        p['OuterFluxRate']=0.0
        p['outer_concentration']=0.0

        self.fixed={}
        
        set_plasmodesmata(db)
        self.p=p
        model_setup(self,db)


    def deriv(self, y, t, db, tissue_maps, ssl, wall_decomposition, fixed_idx):
        p=self.p

        ret = zeros((len(y),))

        cell_map = tissue_maps['cell']
        wall_map = tissue_maps['wall']
        
        A1PI=p['A1']*p['PIAAH']
        A2PA=p['A2']*p['PAUX1']
        A2PL=p['A2']*p['PLAX']
        A3PP=p['A3']*p['PPIN']
        A3Pb=p['A3']*p['Pback']
        B1PI=p['B1']*p['PIAAH']
        B2PA=p['B2']*p['PAUX1']
        B2PL=p['B2']*p['PLAX']
        B3PP=p['B3']*p['PPIN']
        B3Pb=p['B3']*p['Pback']
        
        
        B4PABCB = p['B4']*p['PABCB']
        B5PP_ABCB = p['B5']*p['P_PABCB']
        A4PABCB = 0 # assume no reflux through ABCB
        A5PP_ABCB = 0 # assume no reflux through ABCB
        
        
        Ppla=p['Pplas']

        PIN = db.get_property("PIN")
        
        AUX1 = db.get_property("AUX1")
        LAX = db.get_property("LAX")
        plasmod=db.get_property("plasmodesmata")
        
        ABCB=db.get_property('ABCB')
        ABCB1=db.get_property('ABCB1')
        ABCB4=db.get_property('ABCB4')
        ABCB19=db.get_property('ABCB19')
        for eid in ABCB.keys():
            ABCB[eid]=(ABCB1[eid]+ABCB4[eid]+ABCB19[eid])
        
        

        # model equations
        V = db.get_property('V')
        S = db.get_property('S')
        wall_width=db.get_property('wall_width')
        pauxin=db.get_property('pauxin')

        mesh=get_mesh(db)
        graph=get_graph(db)
        
        
        
        if p['ABCB_scen'] == 1:
            B5PP_ABCB = 0
        elif p['ABCB_scen'] == 2:
            A3PP=0
            B3PP=0
            B4PABCB = 0
        elif p['ABCB_scen'] == 3:
            pass
        elif p['ABCB_scen'] == 4:
            A3PP=0
            B3PP=0
        elif p['ABCB_scen'] == 5:
            B4PABCB = 0
        else:
            raise Exception('Error, ABCB_scen must take a value of 1,2,3,4 or 5')
        

        for wid in self.inner_walls:
            
                
            (eid1,eid2)=wall_decomposition[wid]
            cell1 = graph.source(eid1)
            cell2 = graph.target(eid1)
            index_cell1 = ssl['auxin'].start+cell_map[cell1]
            index_cell2 = ssl['auxin'].start+cell_map[cell2]
            index_wall = ssl['auxin_wall'].start+wall_map[wid]

            Swall=S[wid]
            Vcell1=V[cell1]
            Vcell2=V[cell2]
            Vwall=S[wid]*wall_width[wid]

            #inter-cellular diffusion via Plasmodesmata                
            J1PLA = Ppla*plasmod[wid]*(y[index_cell1]-y[index_cell2]) 
            J2PLA = Ppla*plasmod[wid]*(y[index_cell2]-y[index_cell1])
            
            J1 = (B1PI+B2PA*AUX1[eid1]+B2PL*LAX[eid1]+B3PP*PIN[eid1]+ B3Pb+B4PABCB*ABCB[eid1]+B5PP_ABCB*PIN[eid1]*ABCB[eid1])*y[index_cell1]-(A1PI+A2PA*AUX1[eid1]+A2PL*LAX[eid1]+A3PP*PIN[eid1]+A4PABCB*ABCB[eid1]+A5PP_ABCB*PIN[eid1]*ABCB[eid1]+A3Pb)*y[index_wall]
            J2 = (B1PI+B2PA*AUX1[eid2]+B2PL*LAX[eid2]+B3PP*PIN[eid2]+B4PABCB*ABCB[eid2]+B5PP_ABCB*PIN[eid2]*ABCB[eid2]+B3Pb)*y[index_cell2]-(A1PI+A2PA*AUX1[eid2]+A2PL*LAX[eid2]+A3PP*PIN[eid2]+A4PABCB*ABCB[eid2]+A5PP_ABCB*PIN[eid2]*ABCB[eid2]+A3Pb)*y[index_wall]

            ret[index_cell1] += -Swall/Vcell1 * (J1+J1PLA)
            ret[index_cell2] += -Swall/Vcell2 * (J2+J2PLA)
            ret[index_wall]  += Swall/Vwall*(J1+J2)

        for wid,cid in self.outer_walls.items():

            index_cell = ssl['auxin'].start+cell_map[cid]
            index_wall= ssl['auxin_wall'].start+wall_map[wid]
            Swall=S[wid]
            Vcell=V[cid]
            Vwall=S[wid]*wall_width[wid]
            J1=(B1PI+B2PA+B3Pb)* y[index_cell]-(A1PI+A2PA+A3Pb)*y[index_wall]

            ret[index_cell] += -Swall/Vcell * J1                                     
            ret[index_wall] += Swall/Vwall*J1

            ret[index_wall] += max(0.0,p['OuterFluxRate']*(p['outer_concentration']-y[index_wall]))

        # Calculate effective auxin concentration at each vertex (in limit as vertex volume -> 0)
        # and prescribe fluxes between adjoining cell wall compartments.
        for pid,wids in self.points_to_walls.items():
            top = 0.0
            bottom = 0.0
            for wid in wids:
                index_wall=ssl['auxin_wall'].start+wall_map[wid]
                top += y[index_wall]/S[wid]
                bottom += 1/S[wid]
            Auxin_vertex = top/bottom
            for wid in wids:
                index_wall=ssl['auxin_wall'].start+wall_map[wid]
                ret[index_wall]+=1/S[wid]*(2*p['D_cw']/S[wid]*(Auxin_vertex-y[index_wall]))
        #production and degradation

        for cid in mesh.wisps(2):
            cidx=ssl['auxin'].start+cell_map[cid]
            #cidv=ssl['VENUS'].start+cell_map[cid]
            ret[cidx]+=(pauxin[cid]-p['dauxin']*y[cidx])#/V[cid]
            #ret[cidv]+=p['p2']*(1-(y[cidx]*y[cidv])/(p['q1']+p['q2']*y[cidx]+p['p1']*y[cidx]*y[cidv]))

        for i in fixed_idx:
            ret[i] = 0.0
        return ret

    def set_ss(self, db, tissue_maps, fixed_steady,ssl):
        p=self.p
        plas_flux=db.get_property('plas_flux')
        carrier_flux=db.get_property('carrier_flux')
        combined_flux=db.get_property('combined_flux')
        auxlax_flux=db.get_property('auxlax_flux')
        pin_flux=db.get_property('pin_flux')
        passive_flux=db.get_property('passive_flux')

        A1PI=p['A1']*p['PIAAH']
        A2PA=p['A2']*p['PAUX1']
        A2PL=p['A2']*p['PLAX']
        A3PP=p['A3']*p['PPIN']
        A3Pb=p['A3']*p['Pback']
        B1PI=p['B1']*p['PIAAH']
        B2PA=p['B2']*p['PAUX1']
        B2PL=p['B2']*p['PLAX']
        B3PP=p['B3']*p['PPIN']
        B3Pb=p['B3']*p['Pback']
        Ppla=p['Pplas']
        
        B4PABCB = p['B4']*p['PABCB']
        B5PP_ABCB = p['B5']*p['P_PABCB']
        A4PABCB = 0 # assume no reflux through ABCB
        A5PP_ABCB = 0 # assume no reflux through ABCB
        
        
        if p['ABCB_scen'] == 1:
            B5PP_ABCB = 0
        elif p['ABCB_scen'] == 2:
            A3PP=0
            B3PP=0
            B4PABCB = 0
        elif p['ABCB_scen'] == 3:
            pass
        elif p['ABCB_scen'] == 4:
            A3PP=0
            B3PP=0
        elif p['ABCB_scen'] == 5:
            B4PABCB = 0
        else:
            raise Exception('Error, ABCB_scen must take a value of 1,2,3,4 or 5')
        PIN = db.get_property("PIN")
        
        AUX1 = db.get_property("AUX1")
        LAX = db.get_property("LAX")
        plasmod=db.get_property("plasmodesmata")
        
        auxin = db.get_property("auxin")
        auxin_wall = db.get_property("auxin_wall")

        ABCB=db.get_property('ABCB')
        ABCB1=db.get_property('ABCB1')
        ABCB4=db.get_property('ABCB4')
        ABCB19=db.get_property('ABCB19')
        for eid in ABCB.keys():
            ABCB[eid]=(ABCB1[eid]+ABCB4[eid]+ABCB19[eid])
        
        V = db.get_property('V')
        S = db.get_property('S')
        wall_width=db.get_property('wall_width')
        pauxin=db.get_property('pauxin')

        mesh=get_mesh(db)
        graph=get_graph(db)
        wall_decomposition=get_wall_decomp(db)

        for wid in self.inner_walls:
            
                
            (eid1,eid2)=wall_decomposition[wid]
            sid = graph.source(eid1)
            tid = graph.target(eid1)

            Swall=S[wid]
            Vcell1=V[sid]
            Vcell2=V[tid]
            Vwall=S[wid]*wall_width[wid]


            Jpla = Ppla*plasmod[wid]
            J1PLA = Ppla*plasmod[wid]*(auxin[sid]-auxin[tid]) 
            plas_flux[wid]=J1PLA/S[wid]
            
            J1 = (B1PI+B2PA*AUX1[eid1]+B2PL*LAX[eid1]+B3PP*PIN[eid1]+B4PABCB*ABCB[eid1]+B5PP_ABCB*PIN[eid1]*ABCB[eid1]+ B3Pb)*auxin[sid]\
                -(A1PI+A2PA*AUX1[eid1]+A2PL*LAX[eid1]+A3PP*PIN[eid1]+A4PABCB*ABCB[eid1]+A5PP_ABCB*PIN[eid1]*ABCB[eid1]+ A3Pb)*auxin_wall[wid]
            J2 = (B1PI+B2PA*AUX1[eid2]+B2PL*LAX[eid2]+B3PP*PIN[eid2]+B4PABCB*ABCB[eid2]+B5PP_ABCB*PIN[eid2]*ABCB[eid2]+ B3Pb)*auxin[tid]\
                -(A1PI+A2PA*AUX1[eid2]+A2PL*LAX[eid2]+A3PP*PIN[eid2]+A4PABCB*ABCB[eid2]+A5PP_ABCB*PIN[eid2]*ABCB[eid2]+ A3Pb)*auxin_wall[wid]
            carrier_flux[wid] = (J1-J2)/2*S[wid]

            J1 = (B1PI+B2PA*AUX1[eid1]+B2PL*LAX[eid1])*auxin[sid]-(A1PI+A2PA*AUX1[eid1]+A2PL*LAX[eid1])*auxin_wall[wid]
            J2 = (B1PI+B2PA*AUX1[eid2]+B2PL*LAX[eid2])*auxin[tid]-(A1PI+A2PA*AUX1[eid2]+A2PL*LAX[eid2])*auxin_wall[wid]
            auxlax_flux[wid] = (J1-J2)/2*S[wid]

            J1 = (B3PP*PIN[eid1])*auxin[sid]-(A3PP*PIN[eid1])*auxin_wall[wid]
            J2 = (B3PP*PIN[eid2])*auxin[tid]-(A3PP*PIN[eid2])*auxin_wall[wid]
            pin_flux[wid] = (J1-J2)/2*S[wid]
            
            J1 = (B3Pb)*auxin[sid]-(A3Pb)*auxin_wall[wid]
            J2 = (B3Pb)*auxin[tid]-(A3Pb)*auxin_wall[wid]
            passive_flux[wid] = (J1-J2)/2*S[wid]
            
            combined_flux[wid]=carrier_flux[wid]+plas_flux[wid]

        

        
