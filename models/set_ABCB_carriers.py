from vroot.db_utilities import get_mesh,get_graph,get_wall_decomp,def_property,CellToCoordinates
from numpy import mean,array,median
def set_ABCB_carriers(db):
    
    mesh=get_mesh(db)
    graph=get_graph(db)
    wall_decomposition=get_wall_decomp(db)
    cell_type=db.get_property('cell_type')
    pos=db.get_property('position')

    #add ABCB distributions (individual and separate)
    ABCB1={}
    ABCB4={}
    ABCB19={}
    ABCB={}
    for eid in graph.edges():
        ABCB1[eid]=0
        ABCB4[eid]=0
        ABCB19[eid]=0
        ABCB[eid]=0
        
    for cell in cell_type.keys():
        if cell_type[cell] in [2,3,4,5,8,9,17,18,10,16] :
            for eid in graph.out_edges(cell):
                ABCB1[eid]=1
                ABCB[eid]=1
        if cell_type[cell] in [2,6,7,8,9,15] :
            for eid in graph.out_edges(cell):
                ABCB4[eid]=1
                ABCB[eid]=1
        if cell_type[cell] in [3,4,5,17,18,10,16] :
            for eid in graph.out_edges(cell):
                ABCB19[eid]=1
                ABCB[eid]=1

    db.set_property('ABCB', ABCB)
    db.set_description('ABCB', '')
    db.set_property('ABCB1', ABCB1)
    db.set_description('ABCB1', '')
    db.set_property('ABCB4', ABCB4)
    db.set_description('ABCB4', '')
    db.set_property('ABCB19', ABCB19)
    db.set_description('ABCB19', '')
    
    db.get_property('species_desc')['ABCB1']='edge'
    db.get_property('species_desc')['ABCB19']='edge'
    db.get_property('species_desc')['ABCB4']='edge'
    return db

def set_ABCB_carriers_original(db):
    
    mesh=get_mesh(db)
    graph=get_graph(db)
    wall_decomposition=get_wall_decomp(db)
    cell_type=db.get_property('cell_type')
    pos=db.get_property('position')

    #add ABCB distributions (individual and separate)
    ABCB1={}
    ABCB4={}
    ABCB19={}
    ABCB={}
    for eid in graph.edges():
        ABCB1[eid]=0
        ABCB4[eid]=0
        ABCB19[eid]=0
        ABCB[eid]=0
        
    for cell in cell_type.keys():
        if cell_type[cell] in [2,3,4,5,8,9,17,18,10,16] :
            for eid in graph.out_edges(cell):
                ABCB1[eid]=1
                ABCB[eid]=1
        if cell_type[cell] in [2,6,7,8,9] :
            for eid in graph.out_edges(cell):
                ABCB4[eid]=1
                ABCB[eid]=1
        if cell_type[cell] in [3,4,5,17,18,10,16] :
            for eid in graph.out_edges(cell):
                ABCB19[eid]=1
                ABCB[eid]=1

    db.set_property('ABCB', ABCB)
    db.set_description('ABCB', '')
    db.set_property('ABCB1', ABCB1)
    db.set_description('ABCB1', '')
    db.set_property('ABCB4', ABCB4)
    db.set_description('ABCB4', '')
    db.set_property('ABCB19', ABCB19)
    db.set_description('ABCB19', '')
    
    db.get_property('species_desc')['ABCB1']='edge'
    db.get_property('species_desc')['ABCB19']='edge'
    db.get_property('species_desc')['ABCB4']='edge'
    return db

def simple_knockout(db,propname):
    prop=db.get_property(propname)
    for eid in prop.keys():
        prop[eid]=0.0
    return db
