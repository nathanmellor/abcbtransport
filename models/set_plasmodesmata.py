from vroot.db_utilities import get_mesh,get_graph,get_wall_decomp


def set_plasmodesmata(db):

    mesh=get_mesh(db)
    graph=get_graph(db)
    wall_decomposition=get_wall_decomp(db)
    cell_type=db.get_property('cell_type')
    position=db.get_property('position')
    border_to_coordinates = dict((wid,\
                        [[position[tuple(mesh.borders(1,wid))[0]][0],\
                          position[tuple(mesh.borders(1,wid))[0]][1]],\
                         [position[tuple(mesh.borders(1,wid))[1]][0],\
                          position[tuple(mesh.borders(1,wid))[1]][1]]]\
                        ) for wid in mesh.wisps(1)) 
    plasmod={}
    for wid in mesh.wisps(1):
        plasmod[wid]=0

    #define plasmodesmata permeabilities (n.b. longtitudinal seems to be used synonomously with tangental in Zhu)
    #Primary Tissue (immature)
    vas_tran = 9.92
    vas_tang = 2.42
    vas_cort_tang = 3.08 
    epi_tran = 5.42
    epi_per_RC_tang = 0.83
    endo_tran = 12.58 #n.b. in Zhu the endodermis is referred to as the inner cortex
    endo_cort_tang = 3
    cort_tran = 9.08 #n.b. in Zhu the model 'cortex' is refered to as the outer cortex
    cort_epi_tang = 2.33
    peri_RC_tran = 2.08
    peri_RC_tang = 0.25
    col_RC_tran = 3
    col_RC_tang = 1.33
    col_tang = 3.58
    QC_tang = 2 #n.b Quiescent centre is central cortex initial (CCI) in Zhu
    QC_CE_tang = 3.33 #n.b. cortex endodermal (CE) initial is cortex initial (CI) in Zhu
    CI_col_tran = 4.25
    QC_CI_tran = 3.08
    QC_vas_tran = 3.33 #n.b use value between CCI and vasculature initial (VI)
    CE_endo_tran = 6.33 #nb. this is the this cortex endodermal (CE) and Endodermal initial (EI) in Zhu 
    CI_RCP_tang = 3
    
    for wid,(eid1,eid2) in wall_decomposition.items() :
        cid1=graph.source(eid1)
        cid2=graph.source(eid2)
        #Transverse walls
        if cell_type[cid1] == 2 and cell_type[cid2] == 2: #Epidermis transverse walls
            plasmod[wid] = epi_tran
        if cell_type[cid1] == 3 and cell_type[cid2] == 3: #Endodermis transverse walls
            plasmod[wid] = endo_tran
        if cell_type[cid1] == 4 and cell_type[cid2] == 4: #Cortex transverse walls
            plasmod[wid] = cort_tran
        if cell_type[cid1] == 6 and cell_type[cid2] == 6: #LRC1 transverse walls
            plasmod[wid] = peri_RC_tran
        if cell_type[cid1] == 7 and cell_type[cid2] == 7: #LRC2 transverse walls
            plasmod[wid] = peri_RC_tran
        if cell_type[cid1] == 8 and cell_type[cid2] == 8: #LRC3 transverse walls
            plasmod[wid] = peri_RC_tran
        if cell_type[cid1] == 9 and cell_type[cid2] == 9: #LRC4 transverse walls
            plasmod[wid] = peri_RC_tran
            
        #Tangental walls    
        if cell_type[cid1] == 3 and cell_type[cid2] == 16: #walls between endodermis/inner cortex and stele/vasculature
            plasmod[wid] = vas_cort_tang
        if cell_type[cid1] == 16 and cell_type[cid2] == 3: #walls between endodermis/inner cortex and stele/vasculature
            plasmod[wid] = vas_cort_tang
        if cell_type[cid1] == 3 and cell_type[cid2] == 4: #walls between endodermis/inner cortex and cortex/outer cortex
            plasmod[wid] = endo_cort_tang
        if cell_type[cid1] == 4 and cell_type[cid2] == 3: #walls between endodermis/inner cortex and cortex/outer cortex
            plasmod[wid] = endo_cort_tang
        if cell_type[cid1] == 4 and cell_type[cid2] == 2: #walls between (outer) cortex and epidermis
            plasmod[wid] = cort_epi_tang
        if cell_type[cid1] == 2 and cell_type[cid2] == 4: #walls between (outer) cortex and epidermis
            plasmod[wid] = cort_epi_tang
        if cell_type[cid1] == 2 and cell_type[cid2] > 5 and cell_type[cid2] < 10: #Epidermis and root cap
            plasmod[wid] = epi_per_RC_tang
        if cell_type[cid2] == 2 and cell_type[cid1] > 5 and cell_type[cid1] < 10: #Epidermis and root cap
            plasmod[wid] = epi_per_RC_tang
        if cell_type[cid1] == 6 and cell_type[cid2] == 7: #LRC1 and LRC2
            plasmod[wid] = peri_RC_tang
        if cell_type[cid1] == 7 and cell_type[cid2] == 6: #LRC1 and LRC2
            plasmod[wid] = peri_RC_tang
        if cell_type[cid1] == 7 and cell_type[cid2] == 8: #LRC2 and LRC3
            plasmod[wid] = peri_RC_tang
        if cell_type[cid1] == 8 and cell_type[cid2] == 7: #LRC2 and LRC3
            plasmod[wid] = peri_RC_tang       
        if cell_type[cid1] == 8 and cell_type[cid2] == 9: #LRC3 and LRC4
            plasmod[wid] = peri_RC_tang
        if cell_type[cid1] == 9 and cell_type[cid2] == 8: #LRC3 and LRC4
            plasmod[wid] = peri_RC_tang    
            
        #Stele/vasculature 
        #tangental/longitudinal walls   
        denom=abs(border_to_coordinates[wid][0][1]-border_to_coordinates[wid][1][1])
        if denom==0.0:
            denom=1e-6     
        if cell_type[cid1] == 5 and cell_type[cid2] == 5 and abs(border_to_coordinates[wid][0][0]-border_to_coordinates[wid][1][0])>denom: 
            plasmod[wid] = vas_tang
        if cell_type[cid1] == 16 and cell_type[cid2] == 5: 
            plasmod[wid] = vas_tang
        if cell_type[cid1] == 5 and cell_type[cid2] == 16: 
            plasmod[wid] = vas_tang
        #transverse walls    
        if cell_type[cid1] == 5 and cell_type[cid2] == 5 and abs(border_to_coordinates[wid][0][0]-border_to_coordinates[wid][1][0])<=denom: 
            plasmod[wid] = vas_tran
        if cell_type[cid1] == 16 and cell_type[cid2] == 16 and abs(border_to_coordinates[wid][0][0]-border_to_coordinates[wid][1][0])<=denom: 
            plasmod[wid] = vas_tran
        if cell_type[cid1] == 25 and cell_type[cid2] == 25 and abs(border_to_coordinates[wid][0][0]-border_to_coordinates[wid][1][0])<=denom: 
            plasmod[wid] = vas_tran
         
         #Columella root cap
        if cell_type[cid1] == 11 and cell_type[cid2] == 12: # S1 and S2 transverse walls
            plasmod[wid] = col_RC_tran    
        if cell_type[cid1] == 12 and cell_type[cid2] == 11: # S1 and S2 transverse walls
            plasmod[wid] = col_RC_tran
        if cell_type[cid1] == 10 and cell_type[cid2] == 12: # S1 and S2 transverse walls
            plasmod[wid] = col_RC_tran    
        if cell_type[cid1] == 12 and cell_type[cid2] == 10: # S1 and S2 transverse walls
            plasmod[wid] = col_RC_tran
        if cell_type[cid1] == 12 and cell_type[cid2] == 13: # S2 and S3 transverse walls
            plasmod[wid] = col_RC_tran    
        if cell_type[cid1] == 13 and cell_type[cid2] == 12: # S2 and S3 transverse walls
            plasmod[wid] = col_RC_tran
        if cell_type[cid1] == 13 and cell_type[cid2] == 14: # S3 and S4 transverse walls
            plasmod[wid] = col_RC_tran    
        if cell_type[cid1] == 14 and cell_type[cid2] == 13: # S3 and S4 transverse walls
            plasmod[wid] = col_RC_tran
        if cell_type[cid1] == 14 and cell_type[cid2] == 15 and abs(border_to_coordinates[wid][0][0]-border_to_coordinates[wid][1][0])/denom >1: # S4 and S5 transverse walls
            plasmod[wid] = col_RC_tang
        if cell_type[cid1] == 14 and cell_type[cid2] == 15 and abs(border_to_coordinates[wid][0][0]-border_to_coordinates[wid][1][0])/denom <=1: # S4 and S5 transverse walls
            plasmod[wid] = col_RC_tran    
        if cell_type[cid1] == 15 and cell_type[cid2] == 14 and abs(border_to_coordinates[wid][0][0]-border_to_coordinates[wid][1][0])/denom >1: # S4 and S5 transverse walls
            plasmod[wid] = col_RC_tang
        if cell_type[cid1] == 15 and cell_type[cid2] == 14 and abs(border_to_coordinates[wid][0][0]-border_to_coordinates[wid][1][0])/denom <=1: # S4 and S5 transverse walls
            plasmod[wid] = col_RC_tran         
        if cell_type[cid1] == 11 and cell_type[cid2] == 11: # S1 - Columella root cap transverse walls
            plasmod[wid] = col_RC_tang
        if cell_type[cid1] == 12 and cell_type[cid2] == 12: # S2 - Columella root cap transverse walls
            plasmod[wid] = col_RC_tang
        if cell_type[cid1] == 13 and cell_type[cid2] == 13: # S3 - Columella root cap transverse walls
            plasmod[wid] = col_RC_tang
        if cell_type[cid1] == 14 and cell_type[cid2] == 14:  # S4 - Columella root cap transverse walls
            plasmod[wid] = col_RC_tang
        if cell_type[cid1] == 15 and cell_type[cid2] == 15: # S5 - Columella root cap transverse walls
            plasmod[wid] = col_RC_tang    
        
           
        
        # LRC and columella root cap (S1, S2, S3, S4 and S5)    
        #nb. is doesn't look like Zhu includes a value for the wall between the Columella root cap and LRC. I've used LRC values
        
        #LRC1
        if cell_type[cid1] == 6 and cell_type[cid2] == 15: #LRC1 and S5
            plasmod[wid] = peri_RC_tran
        if cell_type[cid1] == 15 and cell_type[cid2] == 6: #LRC1 and S5
            plasmod[wid] = peri_RC_tran
        if cell_type[cid1] == 6 and cell_type[cid2] == 14: #LRC1 and S4
            plasmod[wid] = peri_RC_tang
        if cell_type[cid1] == 14 and cell_type[cid2] == 6: #LRC1 and S4
            plasmod[wid] = peri_RC_tang    
       
       #LRC2
        if cell_type[cid1] == 7 and cell_type[cid2] == 14: #LRC2 and S4
            plasmod[wid] = peri_RC_tran
        if cell_type[cid1] == 14 and cell_type[cid2] == 7: #LRC2 and S4
            plasmod[wid] = peri_RC_tran
        if cell_type[cid1] == 7 and cell_type[cid2] == 13: #LRC2 and S3
            plasmod[wid] = peri_RC_tang
        if cell_type[cid1] == 13 and cell_type[cid2] == 7: #LRC2 and S3
            plasmod[wid] = peri_RC_tang 
            
       #LRC3
        if cell_type[cid1] == 8 and cell_type[cid2] == 13: #LRC3 and S3
            plasmod[wid] = peri_RC_tran
        if cell_type[cid1] == 13 and cell_type[cid2] == 8: #LRC3 and S3
            plasmod[wid] = peri_RC_tran
        if cell_type[cid1] == 8 and cell_type[cid2] == 12: #LRC3 and S2
            plasmod[wid] = peri_RC_tang
        if cell_type[cid1] == 12 and cell_type[cid2] == 8: #LRC3 and S2
            plasmod[wid] = peri_RC_tang 

       #LRC4
        if cell_type[cid1] == 9 and cell_type[cid2] == 12: #LRC4 and S2
            plasmod[wid] = peri_RC_tran
        if cell_type[cid1] == 12 and cell_type[cid2] == 9: #LRC4 and S2
            plasmod[wid] = peri_RC_tran
        if cell_type[cid1] == 9 and cell_type[cid2] == 11: #LRC4 and S1
            plasmod[wid] = peri_RC_tang
        if cell_type[cid1] == 11 and cell_type[cid2] == 9: #LRC4 and S1
            plasmod[wid] = peri_RC_tang           
        if cell_type[cid1] == 9 and cell_type[cid2] == 10: #LRC4 and S1
            plasmod[wid] = peri_RC_tang
        if cell_type[cid1] == 10 and cell_type[cid2] == 9: #LRC4 and S1
            plasmod[wid] = peri_RC_tang
        #Columella intitials
        if cell_type[cid1] == 10 and cell_type[cid2] == 10: #Columella initials tangental walls
            plasmod[wid] = col_tang
        
        #Quiescent centre
        if cell_type[cid1] == 17 and cell_type[cid2] == 17: #Quiescent centre tangental walls
            plasmod[wid] = QC_tang
        if cell_type[cid1] == 17 and cell_type[cid2] == 18: #Quiescent centre and CE initial tangental walls
            plasmod[wid] = QC_CE_tang
        if cell_type[cid1] == 18 and cell_type[cid2] == 17: #Quiescent centre CE initial tangental walls
            plasmod[wid] = QC_CE_tang
        
        #Columella intitials and Root cap
        if cell_type[cid1] == 11 and cell_type[cid2] == 10: #Columella intitial and S1
            plasmod[wid] = CI_col_tran
        if cell_type[cid1] == 10 and cell_type[cid2] == 11: #Columella intitial and S1
            plasmod[wid] = CI_col_tran
            
       #Quiescent centre and Columella intitials    
        if cell_type[cid1] == 17 and cell_type[cid2] == 10: #Quiescent centre and Columella intitials
            plasmod[wid] = QC_CI_tran
        if cell_type[cid1] == 10 and cell_type[cid2] == 17: #Quiescent centre and Columella intitials
            plasmod[wid] = QC_CI_tran
            
        #Quiescent centre and Stele 
        if cell_type[cid1] == 17 and cell_type[cid2] == 5: 
            plasmod[wid] = QC_vas_tran
        if cell_type[cid1] == 5 and cell_type[cid2] == 17: 
            plasmod[wid] = QC_vas_tran
        if cell_type[cid1] == 17 and cell_type[cid2] == 25: 
            plasmod[wid] = QC_vas_tran
        if cell_type[cid1] == 25 and cell_type[cid2] == 17: 
            plasmod[wid] = QC_vas_tran
        if cell_type[cid1] == 17 and cell_type[cid2] == 16: 
            plasmod[wid] = QC_vas_tran
        if cell_type[cid1] == 16 and cell_type[cid2] == 17: 
            plasmod[wid] = QC_vas_tran
        #CE and endodermal 
        if cell_type[cid1] == 18 and cell_type[cid2] == 3: 
            plasmod[wid] = CE_endo_tran
        if cell_type[cid1] == 3 and cell_type[cid2] == 18: 
            plasmod[wid] = CE_endo_tran
        
        #CE and cortex (assume same value as between CE and endodermis)
        if cell_type[cid1] == 18 and cell_type[cid2] == 4: 
            plasmod[wid] = CE_endo_tran
        if cell_type[cid1] == 4 and cell_type[cid2] == 18: 
            plasmod[wid] = CE_endo_tran
       
       #CE and Stele (assume same value as between Quiescent centre and Stele)
        if cell_type[cid1] == 18 and cell_type[cid2] == 5: 
            plasmod[wid] = QC_vas_tran
        if cell_type[cid1] == 5 and cell_type[cid2] == 18: 
            plasmod[wid] = QC_vas_tran     
        if cell_type[cid1] == 18 and cell_type[cid2] == 16: 
            plasmod[wid] = QC_vas_tran
        if cell_type[cid1] == 16 and cell_type[cid2] == 18: 
            plasmod[wid] = QC_vas_tran
        #CE and Col initials (assume same value as between QC and CE)
        if cell_type[cid1] == 18 and cell_type[cid2] == 10: 
            plasmod[wid] = QC_CE_tang
        if cell_type[cid1] == 10 and cell_type[cid2] == 18: 
            plasmod[wid] = QC_CE_tang
            
        #Epidermis and Col initials 
        if cell_type[cid1] == 10 and cell_type[cid2] == 2: 
            plasmod[wid] = CI_RCP_tang
        if cell_type[cid1] == 2 and cell_type[cid2] == 10: 
            plasmod[wid] = CI_RCP_tang
            
        #Cortex and Col initials (assume same as Col initial and QC)
        if cell_type[cid1] == 10 and cell_type[cid2] == 4: 
            plasmod[wid] = QC_CI_tran
        if cell_type[cid1] == 4 and cell_type[cid2] == 10: 
            plasmod[wid] = QC_CI_tran
            
        #Epidermis and Col root cap (S1) (assume same as Col initial and QC)
        if cell_type[cid1] == 11 and cell_type[cid2] == 2: 
            plasmod[wid] = QC_CI_tran
        if cell_type[cid1] == 2 and cell_type[cid2] == 11: 
            plasmod[wid] = QC_CI_tran
            
        #Epidermis and CE (assume same as epidermis and cortex)
        if cell_type[cid1] == 2 and cell_type[cid2] == 18: 
            plasmod[wid] = cort_epi_tang
        if cell_type[cid1] == 18 and cell_type[cid2] == 2: 
            plasmod[wid] = cort_epi_tang    
    #plasmod[6074] = peri_RC_tang#?
    #plasmod[6072] = peri_RC_tang#?

    db.set_property('plasmodesmata', plasmod)
    db.set_description('plasmodesmata', '')
    db.get_property('species_desc')['plasmodesmata']='wall'
