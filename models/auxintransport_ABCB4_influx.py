import sys
sys.path.append('../')

from numpy import zeros
from math import exp
from vroot.db_utilities import get_mesh, get_graph, get_parameters, set_parameters, get_wall_decomp, get_tissue_maps,def_property
from numpy import size, sqrt,pi
from models.set_plasmodesmata import set_plasmodesmata
from vroot.biochemical import model_setup
from scipy.sparse.linalg import spsolve


from scipy.sparse import lil_matrix

class auxintransport_ABCB4_influx(object):
    """

    Implements AbstractModel
    """
    jacobian_links = {'cells_to_walls':[('auxin','auxin_wall')],'cells_to_cells':[],'intracellular':{}}

    diluted_props = ['auxin','auxin_wall']

    def get_species(self):
        return [('auxin', 'cell'),('auxin_wall','wall')]

    def get_steady_species(self):
        return [('PIN','edge'),('AUX1','edge'),('LAX','edge'),('auxin', 'cell'),('auxin_wall','wall'),('VENUS','cell'),
                ('auxin_point','point'),('plas_flux','wall'),('carrier_flux','wall'),('combined_flux','wall'),
                ('auxlax_flux','wall'),('pin_flux','wall'),('passive_flux','wall'),('ABCB','edge'),
                ('ABCB1','edge'),('ABCB4','edge'),('ABCB19','edge')]

    def set_default_parameters(self, db):
        p={}

        # Parameters governing auxin transport
        p['pHapo']=5.3
        p['pHcyt']=7.2
        p['pK']=4.8
        p['Voltage']=-0.120
        p['Temp']=300
        p['Rconst']=8.31
        p['Fdconst']=96500
        p['phi']=p['Fdconst']*p['Voltage']/(p['Rconst']*p['Temp'])
        # Parameters governing auxin transport
        p['A1']=1/(1+pow(10,p['pHapo']-p['pK'])) # A1, A2, A3, B1, B2, B3 are dimensionless
        p['A2']=(1-p['A1'])*p['phi']/(exp(p['phi'])-1)
        p['A3']=-(1-p['A1'])*p['phi']/(exp(-p['phi'])-1)
        p['A4']=-(1-p['A1'])*p['phi']/(exp(-p['phi'])-1)
        p['A5']=-(1-p['A1'])*p['phi']/(exp(-p['phi'])-1)

        p['B1']=1/(1+pow(10,p['pHcyt']-p['pK']))
        p['B2']=-(1-p['B1'])*p['phi']/(exp(-p['phi'])-1)
        p['B3']=(1-p['B1'])*p['phi']/(exp(p['phi'])-1)
        p['B4']=(1-p['B1'])*p['phi']/(exp(p['phi'])-1)
        p['B5']=(1-p['B1'])*p['phi']/(exp(p['phi'])-1)


        p['D_cw']= 32.0   # Diffusion rate in cell wall (microns^2 sec^(-1)) from Kramer 2007.


        # The permeability coefficients 
        p['PIAAH']=  0.56 # Diffusion coefficient of IAAH (micrometers.sec-1)
        p['PAUX1']=0.56 # Permeability of anionic auxin due to AUX1 active influx carriers (for an AUX1 concentration equal to 1). (micrometers.sec-1)
        p['PLAX']= 0.56 # Permeability of anionic auxin due to LAX active influx carriers (for an LAX concentration equal to 1). (micrometers.sec-1)
        p['PPIN']=  0.56  # Permeability of anionic auxin due to active efflux carriers (for a PIN concentration equal to 1).   (micrometers.sec-1)    
        p['Pback']= 0 # Background efflux permeability
        p['Pplas'] = 8.0/9.92 # Permeability of Plasmodesmata (micrometers.sec-1) 8 in stele (Rustchow et al, 2011) / Plasmodesmata density in transvere wall of Stele
        
        p['PABCB']=  0.56  # Permeability of anionic auxin due to active efflux carriers (for an ABCB concentration equal to 1).   (micrometers.sec-1)  
        p['PABCB4']=  0.56  # Permeability of anionic auxin due to active influx carriers (for an ABCB concentration equal to 1).   (micrometers.sec-1)  
        p['P_PABCB']=  0.56  # Permeability of anionic auxin due to active efflux of PIN and ABCB working in unison   (micrometers.sec-1)
        p['ABCB_scen'] = 1  

        # Parameters governing DII-VENUS degradation (estimated in Band et al. PNAS (2012)). 
        p['p1']=0.06   #  parameter [Auxin-TIR1-VENUS]u/[TIR1]T
        p['p2']=0.001#0.0053/60 # parameter delta/[VENUS]u - too slow????
        p['q1']=0.91   #  parameter [TIR1]u/[TIR1]T
        p['q2']=0.03   #  parameter [Auxin-TIR1]u/[TIR1]T

        Auxinupstream=1  # Auxin concentration in stele at shootward end of the tissue.
        Venusinit=0.01  # Initially set DII-VENUS concentrations to be small.

        # Auxin production and degradation: 
        p['dauxin']=0.001
        p['pauxin']=('cell_type',{17:0.01,10:0.01,6:0.01,7:0.01,'else':0.001})
        
        #exogenous auxin
        p['outer_concentration']=0.0
        

        self.fixed={'auxin':[('cid in border.keys() and cell_type[cid] in [2,3,4,16]',0.0),('cid in border.keys() and cell_type[cid] in [5,25]',Auxinupstream)]}
        #self.fixed={}
        set_plasmodesmata(db)
        self.p=p
        model_setup(self,db)



    def deriv(self, y, t, db, tissue_maps, ssl, wall_decomposition, fixed_idx):

        ret = zeros((len(y),))

        return ret

    def set_ss(self, db, tissue_maps, fixed_idx,ssl):

        p=self.p
        
        
        A1PI=p['A1']*p['PIAAH']
        A2PA=p['A2']*p['PAUX1']
        A2PL=p['A2']*p['PLAX']
        A3PP=p['A3']*p['PPIN']
        A3Pb=p['A3']*p['Pback']
        B1PI=p['B1']*p['PIAAH']
        B2PA=p['B2']*p['PAUX1']
        B2PL=p['B2']*p['PLAX']
        B3PP=p['B3']*p['PPIN']
        B3Pb=p['B3']*p['Pback']
        
        A2P4=p['A2']*p['PABCB4']
        B2P4=0 # Assume no efflux via ABCB4
        
        

        B4PABCB = p['B4']*p['PABCB']
        B5PP_ABCB = p['B5']*p['P_PABCB']
        A4PABCB = 0 # assume no influx through ABCB1,19
        A5PP_ABCB = 0 # assume no influx through ABCB1,19
        Ppla=p['Pplas']
        
        pauxin=db.get_property('pauxin')

        S = db.get_property('S')
        V = db.get_property('V')
        wall_width=db.get_property('wall_width')
        graph=get_graph(db)
        mesh = get_mesh(db)

        auxin=db.get_property('auxin')
        auxin_wall=db.get_property('auxin_wall')
        


        PIN = db.get_property("PIN")
        AUX1 = db.get_property("AUX1")
        LAX = db.get_property("LAX")
        plasmod=db.get_property("plasmodesmata")
        cell_type=db.get_property('cell_type')

        ABCB=db.get_property('ABCB')
        ABCB1=db.get_property('ABCB1')
        ABCB4=db.get_property('ABCB4')
        ABCB19=db.get_property('ABCB19')
        for eid in ABCB.keys():
            ABCB[eid]=(ABCB1[eid]+ABCB19[eid])

        cell_map = tissue_maps['cell']
        wall_map = tissue_maps['wall']
        point_map = tissue_maps['point']
        wall_decomposition=get_wall_decomp(db)
        
        
        c_off = len(list(cell_map.keys()))
        w_off = len(list(wall_map.keys()))
        v_off = len(list(point_map.keys()))

        if p['D_cw']>0.0:
            N = c_off+w_off+v_off
        else:
            N = c_off+w_off
            
        J = lil_matrix((N,N))
        r = zeros((N,))
        
        #set fluxes  
          
        for wid in self.inner_walls:
            widx = ssl['auxin_wall'].start + wall_map[wid]

            eid1 = wall_decomposition[wid][0]
            eid2 = wall_decomposition[wid][1]
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            sidx = ssl['auxin'].start+cell_map[sid]
            tidx = ssl['auxin'].start+cell_map[tid]
            

            
            if p['ABCB_scen'] == 1:
                B5PP_ABCB = 0
            elif p['ABCB_scen'] == 2:
                A3PP=0
                B3PP=0
                B4PABCB = 0
            elif p['ABCB_scen'] == 3:
                pass
            elif p['ABCB_scen'] == 4:
                A3PP=0
                B3PP=0
            elif p['ABCB_scen'] == 5:
                B4PABCB = 0
            else:
                raise Exception('Error, ABCB_scen must take a value of 1,2,3,4 or 5')
            
            #carrier mediated transport and diffusion
            Jsw = B1PI+B2PA*AUX1[eid1]+B2PL*LAX[eid1]+B2P4*ABCB4[eid1]+B3PP*PIN[eid1]+B4PABCB*ABCB[eid1]+B5PP_ABCB*PIN[eid1]*ABCB[eid1]+B3Pb
            Jtw = B1PI+B2PA*AUX1[eid2]+B2PL*LAX[eid2]+B2P4*ABCB4[eid2]+B3PP*PIN[eid2]+B4PABCB*ABCB[eid2]+B5PP_ABCB*PIN[eid2]*ABCB[eid2]+B3Pb
            Jws = A1PI+A2PA*AUX1[eid1]+A2PL*LAX[eid1]+A2P4*ABCB4[eid1]+A3PP*PIN[eid1]+A4PABCB*ABCB[eid1]+A5PP_ABCB*PIN[eid1]*ABCB[eid1]+A3Pb
            Jwt = A1PI+A2PA*AUX1[eid2]+A2PL*LAX[eid2]+A2P4*ABCB4[eid2]+A3PP*PIN[eid2]+A4PABCB*ABCB[eid2]+A5PP_ABCB*PIN[eid2]*ABCB[eid2]+A3Pb

            J[sidx, sidx] += -(S[wid]/V[sid])*Jsw
            J[sidx, widx] += (S[wid]/V[sid])*Jws
            J[widx, sidx] += (1.0/wall_width[wid])*Jsw
            J[widx, widx] += -(1.0/wall_width[wid])*Jws

            J[tidx, tidx] += -(S[wid]/V[tid])*Jtw
            J[tidx, widx] += (S[wid]/V[tid])*Jwt
            J[widx, tidx] += (1.0/wall_width[wid])*Jtw
            J[widx, widx] += -(1.0/wall_width[wid])*Jwt
            
            #inter-cellular diffusion via Plasmodesmata
            Jpla = Ppla*plasmod[wid]

            J[sidx, sidx] += -(S[wid]/V[sid])*Jpla
            J[sidx, tidx] += (S[wid]/V[sid])*Jpla
            J[tidx, sidx] += (S[wid]/V[tid])*Jpla
            J[tidx, tidx] += -(S[wid]/V[tid])*Jpla
        
        for wid,cid in self.outer_walls.items():
        
            widx = ssl['auxin_wall'].start + wall_map[wid]
            sidx = ssl['auxin'].start + cell_map[cid]
            
            #carrier mediated transport and diffusion
            Jsw= B1PI+B2PA+B3Pb
            Jws= A1PI+A2PA+A3Pb
            
            J[sidx, sidx] += -(S[wid]/V[cid])*Jsw
            J[sidx, widx] += (S[wid]/V[cid])*Jws
            J[widx, sidx] += (1.0/wall_width[wid])*Jsw
            J[widx, widx] += -(1.0/wall_width[wid])*Jws
        
        #production / degradation
        for cid in mesh.wisps(mesh.degree()):
            cidx = ssl['auxin'].start + cell_map[cid]
            J[cidx, cidx] -= p['dauxin']#/V[cid]
            r[cidx] += pauxin[cid]#/V[cid]
        
        # prescribe fluxes between adjoining cell wall compartments
        if p['D_cw']>0.0:
            for pid,wids in self.points_to_walls.items():
                pidx=c_off+w_off+point_map[pid]
                for wid in wids:
                    widx=ssl['auxin_wall'].start+wall_map[wid]
                    J[pidx,pidx] += -(1.0/wall_width[wid])*(2*p['D_cw']/S[wid])
                    J[pidx,widx] += (1.0/wall_width[wid])*(2*p['D_cw']/S[wid])
                    J[widx,widx] += -(1.0/S[wid])*(2*p['D_cw']/S[wid])
                    J[widx,pidx] += (1.0/S[wid])*(2*p['D_cw']/S[wid])


        # fixed properties    
        reverse_map={}
        reverse_ids={}
        for cid,val in auxin.items():
            reverse_map[ssl['auxin'].start+cell_map[cid]]=val
            reverse_ids[ssl['auxin'].start+cell_map[cid]]=cid
        for wid,val in auxin_wall.items():
            reverse_map[ssl['auxin_wall'].start+wall_map[wid]]=val
            reverse_ids[ssl['auxin_wall'].start+wall_map[wid]]=wid
            
        
        for i in fixed_idx:
            J[i, :] *=0
            J[i, i] = 1
            r[i]=0
            if cell_type[reverse_ids[i]] in [2,3,4,16]:
                ncids=list(mesh.border_neighbors(2,reverse_ids[i]))
                found=False
                for xcid in ncids:
                    if cell_type[reverse_ids[i]]==cell_type[xcid]:
                        nid=xcid
                        found=True
                if found==False:
                    print('!!!boundary neighbour not found!!!')

                J[i, ssl['auxin'].start+cell_map[nid]] = -1
            else:
                r[i] = -reverse_map[i]
        
        #fixed outer boundary
        if p['outer_concentration']>0.0:
            for wid in self.outer_walls.keys():
                widx = ssl['auxin_wall'].start + wall_map[wid]
                J[widx, :] *=0
                J[widx,widx]=1
                r[widx]=-p['outer_concentration']
        
        #solve
        y = -spsolve(J.tocsr(), r)

        #assign values
        for cid in auxin.keys():
            auxin[cid]=y[ssl['auxin'].start+cell_map[cid]]        

        for wid in auxin_wall.keys():
            auxin_wall[wid]=y[ssl['auxin_wall'].start + wall_map[wid]]
            
        auxin_point=db.get_property('auxin_point')
        if p['D_cw']>0.0:
            for pid in auxin_point.keys():
                auxin_point[pid]=y[c_off+w_off + point_map[pid]]

        VENUS=db.get_property('VENUS')
        mean_auxin=1#mean(auxin.values())
        for cid,val in auxin.items():
            if val != 0:
                VENUS[cid]=((p['q1']*mean_auxin)/val+p['q2'])/((1-p['p1']))


        #assign fluxes for output
        plas_flux=db.get_property('plas_flux')
        carrier_flux=db.get_property('carrier_flux')
        combined_flux=db.get_property('combined_flux')
        auxlax_flux=db.get_property('auxlax_flux')
        pin_flux=db.get_property('pin_flux')
        passive_flux=db.get_property('passive_flux')
        for wid in self.inner_walls:
            
            eid1 = wall_decomposition[wid][0]
            if graph.source(eid1)>graph.target(eid1):
                eid2 = eid1
                eid1 = wall_decomposition[wid][1]
            else:
                eid2 = wall_decomposition[wid][1]
            

            
            sid = graph.source(eid1)
            tid = graph.target(eid1)

            Jpla = Ppla*plasmod[wid]
            J1PLA = Ppla*plasmod[wid]*(auxin[sid]-auxin[tid]) 
            plas_flux[wid]=J1PLA*S[wid]
            
            J1 = (B1PI+B2PA*AUX1[eid1]+B2PL*LAX[eid1]+B2P4*ABCB4[eid1]+B3PP*PIN[eid1]+B4PABCB*ABCB[eid1]+B5PP_ABCB*PIN[eid1]*ABCB[eid1]+ B3Pb)*auxin[sid]\
                -(A1PI+A2PA*AUX1[eid1]+A2PL*LAX[eid1]+A2P4*ABCB4[eid1]+A3PP*PIN[eid1]+A4PABCB*ABCB[eid1]+A5PP_ABCB*PIN[eid1]*ABCB[eid1]+ A3Pb)*auxin_wall[wid]
            J2 = (B1PI+B2PA*AUX1[eid2]+B2PL*LAX[eid2]+B2P4*ABCB4[eid2]+B3PP*PIN[eid2]+B4PABCB*ABCB[eid2]+B5PP_ABCB*PIN[eid2]*ABCB[eid2]+ B3Pb)*auxin[tid]\
                -(A1PI+A2PA*AUX1[eid2]+A2PL*LAX[eid2]+A2P4*ABCB4[eid2]+A3PP*PIN[eid2]+A4PABCB*ABCB[eid2]+A5PP_ABCB*PIN[eid2]*ABCB[eid2]+ A3Pb)*auxin_wall[wid]
            carrier_flux[wid] = (J1-J2)/2*S[wid]

            J1 = (B2PA*AUX1[eid1]+B2PL*LAX[eid1])*auxin[sid]-(A2PA*AUX1[eid1]+A2PL*LAX[eid1])*auxin_wall[wid]
            J2 = (B2PA*AUX1[eid2]+B2PL*LAX[eid2])*auxin[tid]-(A2PA*AUX1[eid2]+A2PL*LAX[eid2])*auxin_wall[wid]
            auxlax_flux[wid] = (J1-J2)/2*S[wid]

            J1 = (B3PP*PIN[eid1])*auxin[sid]-(A3PP*PIN[eid1])*auxin_wall[wid]
            J2 = (B3PP*PIN[eid2])*auxin[tid]-(A3PP*PIN[eid2])*auxin_wall[wid]
            pin_flux[wid] = (J1-J2)/2*S[wid]
            
            J1 = (B1PI)*auxin[sid]-(B1PI)*auxin_wall[wid]
            J2 = (B1PI)*auxin[tid]-(B1PI)*auxin_wall[wid]
            passive_flux[wid] = (J1-J2)/2*S[wid]
            
            combined_flux[wid]=carrier_flux[wid]+plas_flux[wid]
