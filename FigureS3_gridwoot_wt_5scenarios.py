
from vroot.simulation import Simulation
from models.auxintransport_ABCB import auxintransport_ABCB
from models.set_ABCB_carriers import set_ABCB_carriers

for i in range(1,6):

    tissuename = "Tissues/gridroot_V4.zip"
    sim=Simulation(tissuename,'wt_scen_%d' % i,biochemical=auxintransport_ABCB, output = False )
    set_ABCB_carriers(sim.get_db())
    sim.set_param_values({'ABCB_scen':i})
    sim.run_simulation(1)
    sim.plot_figure('auxin',{'auxin':[0,sim.get_max('auxin')]},(1,1))
    sim.plot_figure('VENUS',{'VENUS':[0,sim.get_max('VENUS')]},(1,1))
    sim.plot_flux('combined_flux','combined_flux',0.5,0.5,'b')
