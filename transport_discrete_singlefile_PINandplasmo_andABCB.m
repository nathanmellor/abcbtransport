function Acdot=transport_discrete_singlefile_PINandplasmo_andABCB(t,conc,Nx,PIAAH,PAUX1,PPIN,Pplas,PABCB,Ppinabcb,l,lambda,A1,A2,A3,B1,B2,B3,cup,cend)

c=conc(1:Nx);
f=conc(Nx+1:2*Nx);
fm=conc(2*Nx+1);
cdot=zeros(Nx,1);
fdot=zeros(Nx,1);

Jfc(2:Nx)=(A1*PIAAH+A2*PAUX1)*f(1:Nx-1)-(B1*PIAAH+B2*PAUX1+(1-B1)*PABCB)*c(2:Nx);
Jfc(1)=(A1*PIAAH+A2*PAUX1)*fm-(B1*PIAAH+B2*PAUX1+(1-B1)*PABCB)*c(1);
Jcf(1:Nx)=(B1*PIAAH+B2*PAUX1+B3*PPIN+(1-B1)*PABCB+B3*Ppinabcb)*c(1:Nx)-(A1*PIAAH+A2*PAUX1+A3*PPIN+A3*Ppinabcb)*f(1:Nx);
Jcfm=(B1*PIAAH+B2*PAUX1+B3*PPIN+(1-B1)*PABCB+B3*Ppinabcb)*cup-(A1*PIAAH+A2*PAUX1+A3*PPIN+A3*Ppinabcb)*fm;
Jfcend=(A1*PIAAH+A2*PAUX1)*f(Nx)-(B1*PIAAH+B2*PAUX1+(1-B1)*PABCB)*cend;

Jplas(2:Nx)=Pplas*(c(1:Nx-1)-c(2:Nx));
Jplas(1)=Pplas*(cup-c(1));
Jplasend=Pplas*(c(Nx)-cend);


cdot(1:Nx-1)=1/l*(Jfc(1:Nx-1)-Jcf(1:Nx-1)+Jplas(1:Nx-1)-Jplas(2:Nx));
cdot(Nx)=1/l*(Jfc(Nx)-Jcf(Nx)+Jplas(Nx)-Jplasend);

fdot(1:Nx-1)=1/lambda*(Jcf(1:Nx-1)-Jfc(2:Nx));
fdot(Nx)=1/lambda*(Jcf(Nx)-Jfcend);
fmdot=1/lambda*(Jcfm-Jfc(1));

Acdot=[cdot;fdot;fmdot];