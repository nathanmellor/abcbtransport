Nx=99;

l=20;
L=20*50;
lambda=0.5;
epsilon=l/L;

% Transport rates
PIAAH=0.56;

A1=0.24;
A2=3.57;
A3=0.034;
B1=0.004;
B2=0.045;
B3=4.68;
cup=1;
cend=0;
tmax=2000;
numframes=100;
tspacing=tmax/numframes;

xgrid_discrete=[0:1/Nx:1];
cellnumbervec=[1:Nx+1];


PABCB=0.0;
Ppinabcb=0.0;

% PIN only
Pplas=8.0;
PAUX1=0;
PPIN=0.56;
PABCB=0.0;
Ppinabcb=0.0;
[tdiscrete,cvec_PINalone]=ode15s(@transport_discrete_singlefile_PINandplasmo_andABCB,[0:tspacing:tmax],zeros(1,2*Nx+1),[],Nx,PIAAH,PAUX1,PPIN,Pplas,PABCB,Ppinabcb,l,lambda,A1,A2,A3,B1,B2,B3,cup,cend);

% PIN and ABCB no synergistic flux (Scenario I)
Pplas=8.0;
PAUX1=0;
PPIN=0.56;
PABCB=0.56;
Ppinabcb=0.0;
[tdiscrete,cvec_PINabcb]=ode15s(@transport_discrete_singlefile_PINandplasmo_andABCB,[0:tspacing:tmax],zeros(1,2*Nx+1),[],Nx,PIAAH,PAUX1,PPIN,Pplas,PABCB,Ppinabcb,l,lambda,A1,A2,A3,B1,B2,B3,cup,cend);

% synergistic flux only (Scenario II)
Pplas=8.0;
PAUX1=0;
PPIN=0.0;
PABCB=0.0;
Ppinabcb=0.56;
[tdiscrete,cvec_SYN]=ode15s(@transport_discrete_singlefile_PINandplasmo_andABCB,[0:tspacing:tmax],zeros(1,2*Nx+1),[],Nx,PIAAH,PAUX1,PPIN,Pplas,PABCB,Ppinabcb,l,lambda,A1,A2,A3,B1,B2,B3,cup,cend);


% PIN and ABCB plus synergistic flux (Scenario III)
Pplas=8.0;
PAUX1=0;
PPIN=0.56;
PABCB=0.56;
Ppinabcb=0.56;
[tdiscrete,cvec_PINabcbSYN]=ode15s(@transport_discrete_singlefile_PINandplasmo_andABCB,[0:tspacing:tmax],zeros(1,2*Nx+1),[],Nx,PIAAH,PAUX1,PPIN,Pplas,PABCB,Ppinabcb,l,lambda,A1,A2,A3,B1,B2,B3,cup,cend);

% PIN and plus synergistic flux only (Scenario V)
Pplas=8.0;
PAUX1=0;
PPIN=0.56;
PABCB=0.0;
Ppinabcb=0.56;
[tdiscrete,cvec_PINSYN]=ode15s(@transport_discrete_singlefile_PINandplasmo_andABCB,[0:tspacing:tmax],zeros(1,2*Nx+1),[],Nx,PIAAH,PAUX1,PPIN,Pplas,PABCB,Ppinabcb,l,lambda,A1,A2,A3,B1,B2,B3,cup,cend);


cyt_PINalone=cvec_PINalone(:,1:Nx);
cyt_PINabcb=cvec_PINabcb(:,1:Nx);
cyt_PINabcbSYN=cvec_PINabcbSYN(:,1:Nx);
cyt_PINSYN=cvec_PINSYN(:,1:Nx);
cyt_SYN=cvec_SYN(:,1:Nx);

%cyt_Pplas10=cvec_Pplas10(:,1:Nx);
%cyt_Pplas5=cvec_Pplas5(:,1:Nx);
%cyt_Pplas1=cvec_Pplas1(:,1:Nx);


figure
hold on
for j=21:20:101
    subplot(1,5,(j-1)/20)
    hold on
     axis([0 100 0 1.1])
    

plot(cellnumbervec,[1,cyt_PINabcb(j,:)],'LineStyle','-','Color','r','LineWidth',2);
plot(cellnumbervec,[1,cyt_PINalone(j,:)],'LineStyle','-','Color','b','LineWidth',2);
plot(cellnumbervec,[1,cyt_PINabcbSYN(j,:)],'LineStyle','-','Color','g','LineWidth',2);
plot(cellnumbervec,[1,cyt_PINSYN(j,:)],'LineStyle','-','Color','k','LineWidth',2);

title(['Time=',num2str((j*tspacing-tspacing),'% 10.0f s')],'Interpreter','latex','FontSize',14)
xlabel('Cell number','Interpreter','latex','FontSize',14);
end
subplot(1,5,1)
ylabel('Concentration','Interpreter','latex','FontSize',14);
legend1=legend('Scenario I or IV','Scenario II','Scenario III','Scenario V');
set(legend1,'Interpreter','latex','Location','northeast','FontSize',12)


% PIN and ABCB plus synergistic flux
Pplas=8.0;
PAUX1=0;
PPIN=0.56;
PABCB=0.28;
Ppinabcb=0.56;
[tdiscrete,cvec_PINABCB1]=ode15s(@transport_discrete_singlefile_PINandplasmo_andABCB,[0:tspacing:tmax],zeros(1,2*Nx+1),[],Nx,PIAAH,PAUX1,PPIN,Pplas,PABCB,Ppinabcb,l,lambda,A1,A2,A3,B1,B2,B3,cup,cend);

PABCB=0.56;
Ppinabcb=0.56;

[tdiscrete,cvec_PINABCB2]=ode15s(@transport_discrete_singlefile_PINandplasmo_andABCB,[0:tspacing:tmax],zeros(1,2*Nx+1),[],Nx,PIAAH,PAUX1,PPIN,Pplas,PABCB,Ppinabcb,l,lambda,A1,A2,A3,B1,B2,B3,cup,cend);

PABCB=1.12;
Ppinabcb=0.56;

[tdiscrete,cvec_PINABCB3]=ode15s(@transport_discrete_singlefile_PINandplasmo_andABCB,[0:tspacing:tmax],zeros(1,2*Nx+1),[],Nx,PIAAH,PAUX1,PPIN,Pplas,PABCB,Ppinabcb,l,lambda,A1,A2,A3,B1,B2,B3,cup,cend);

PABCB=0.56;
Ppinabcb=0.28;
[tdiscrete,cvec_PINABCB4]=ode15s(@transport_discrete_singlefile_PINandplasmo_andABCB,[0:tspacing:tmax],zeros(1,2*Nx+1),[],Nx,PIAAH,PAUX1,PPIN,Pplas,PABCB,Ppinabcb,l,lambda,A1,A2,A3,B1,B2,B3,cup,cend);

PABCB=0.56;
Ppinabcb=0.56;

[tdiscrete,cvec_PINABCB5]=ode15s(@transport_discrete_singlefile_PINandplasmo_andABCB,[0:tspacing:tmax],zeros(1,2*Nx+1),[],Nx,PIAAH,PAUX1,PPIN,Pplas,PABCB,Ppinabcb,l,lambda,A1,A2,A3,B1,B2,B3,cup,cend);

PABCB=0.56;
Ppinabcb=1.12;

[tdiscrete,cvec_PINABCB6]=ode15s(@transport_discrete_singlefile_PINandplasmo_andABCB,[0:tspacing:tmax],zeros(1,2*Nx+1),[],Nx,PIAAH,PAUX1,PPIN,Pplas,PABCB,Ppinabcb,l,lambda,A1,A2,A3,B1,B2,B3,cup,cend);

PABCB=0.56;
Ppinabcb=-0.56;
[tdiscrete,cvec_PINABCB7]=ode15s(@transport_discrete_singlefile_PINandplasmo_andABCB,[0:tspacing:tmax],zeros(1,2*Nx+1),[],Nx,PIAAH,PAUX1,PPIN,Pplas,PABCB,Ppinabcb,l,lambda,A1,A2,A3,B1,B2,B3,cup,cend);

PABCB=0;
Ppinabcb=0;

[tdiscrete,cvec_PINABCB8]=ode15s(@transport_discrete_singlefile_PINandplasmo_andABCB,[0:tspacing:tmax],zeros(1,2*Nx+1),[],Nx,PIAAH,PAUX1,PPIN,Pplas,PABCB,Ppinabcb,l,lambda,A1,A2,A3,B1,B2,B3,cup,cend);


cyt_PINABCB1=cvec_PINABCB1(:,1:Nx);
cyt_PINABCB2=cvec_PINABCB2(:,1:Nx);
cyt_PINABCB3=cvec_PINABCB3(:,1:Nx);
cyt_PINABCB4=cvec_PINABCB4(:,1:Nx);
cyt_PINABCB5=cvec_PINABCB5(:,1:Nx);
cyt_PINABCB6=cvec_PINABCB6(:,1:Nx);
cyt_PINABCB7=cvec_PINABCB7(:,1:Nx);
cyt_PINABCB8=cvec_PINABCB8(:,1:Nx);

figure
hold on
for j=21:20:101
    subplot(3,5,(j-1)/20)
    hold on
     axis([0 100 0 1.1])
    
plot(cellnumbervec,[1,cyt_PINABCB1(j,:)],'LineStyle','-','Color','b','LineWidth',2);
plot(cellnumbervec,[1,cyt_PINABCB2(j,:)],'LineStyle','-','Color','r','LineWidth',2);
plot(cellnumbervec,[1,cyt_PINABCB3(j,:)],'LineStyle','-','Color','g','LineWidth',2);

title(['Time=',num2str((j*tspacing-tspacing),'% 10.0f s')],'Interpreter','latex','FontSize',14)
xlabel('Cell number','Interpreter','latex','FontSize',14);

    subplot(3,5,5+(j-1)/20)
    hold on
     axis([0 100 0 1.1])
    
plot(cellnumbervec,[1,cyt_PINABCB4(j,:)],'LineStyle','-','Color','b','LineWidth',2);
plot(cellnumbervec,[1,cyt_PINABCB5(j,:)],'LineStyle','-','Color','r','LineWidth',2);
plot(cellnumbervec,[1,cyt_PINABCB6(j,:)],'LineStyle','-','Color','g','LineWidth',2);

title(['Time=',num2str((j*tspacing-tspacing),'% 10.0f s')],'Interpreter','latex','FontSize',14)
xlabel('Cell number','Interpreter','latex','FontSize',14);

subplot(3,5,10+(j-1)/20)
    hold on
     axis([0 100 0 1.1])
    
plot(cellnumbervec,[1,cyt_PINABCB7(j,:)],'LineStyle','-','Color','b','LineWidth',2);
plot(cellnumbervec,[1,cyt_PINABCB8(j,:)],'LineStyle','-','Color','r','LineWidth',2);


title(['Time=',num2str((j*tspacing-tspacing),'% 10.0f s')],'Interpreter','latex','FontSize',14)
xlabel('Cell number','Interpreter','latex','FontSize',14);
end
subplot(3,5,1)
ylabel('Concentration','Interpreter','latex','FontSize',14);
legend1=legend('$P_{ABCB}=0.28$','$P_{ABCB}=0.56$','$P_{ABCB}=1.12$');
set(legend1,'Interpreter','latex','Location','northeast','FontSize',12)
subplot(3,5,6)
ylabel('Concentration','Interpreter','latex','FontSize',14);
legend1=legend('$P_{SYN}=0.28$','$P_{SYN}=0.56$','$P_{SYN}=1.12$');
set(legend1,'Interpreter','latex','Location','northeast','FontSize',12)
subplot(3,5,11)
ylabel('Concentration','Interpreter','latex','FontSize',14);
legend1=legend('$P_{ABCB}=0.56$, $P_{SYN}=-0.56$','$P_{ABCB}=0$, $P_{SYN}=-0$');
set(legend1,'Interpreter','latex','Location','northeast','FontSize',12)



